﻿using GameDesign.Models;
using GameDesign.Rules;
using Microsoft.Xna.Framework;

namespace GameDesign.Managers
{
    /// <summary>
    /// The boss level
    /// </summary>
    internal class BossLevel : Level
    {
        /// <summary>
        /// Is boss spawned
        /// </summary>
        private bool bossSpawned = false;

        /// <summary>
        /// Initializes a new instance of the BossLevel class
        /// </summary>
        /// <param name="playerSpawnPoint">The spawn point for the boss</param>
        public BossLevel(Vector2 playerSpawnPoint) : base(playerSpawnPoint)
        {
            this.maxEnemies = 8;
            this.minEnemies = 3;
            this.enemyAcceleration = 6f;
            this.enemySpeed = 4.5f;
            this.enemyDifficulty = 2;
        }

        /// <summary>
        /// Spawn Enemies
        /// </summary>
        /// <param name="gameTime">The GameTime object</param>
        protected override void EnemySpawner(GameTime gameTime)
        {
            if (!this.bossSpawned)
            {
                Boss boss = new Boss(new Vector2(Globals.WIDTH - 200, Globals.HEIGHT / 2), MathHelper.ToRadians(0));
                EntityManager.Add(boss);
                this.bossSpawned = true;
            }
        }
    }
}