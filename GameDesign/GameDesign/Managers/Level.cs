﻿using System;
using GameDesign.Helpers;
using GameDesign.Models;
using GameDesign.Rules;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameDesign.Managers
{
    /// <summary>
    /// Contains all information about a level
    /// </summary>
    internal class Level
    {
        /// <summary>
        /// Level settings
        /// </summary>
        protected int
            enemiesToBeat,
            enemyDifficulty = 1,
            minEnemies,
            maxEnemies,
            enemyCount,
            playerLives;

        /// <summary>
        /// Level states
        /// </summary>
        protected bool
            levelComplete = false,
            gameOver = false;

        /// <summary>
        /// On Screen Display
        /// </summary>
        protected OnScreenDisplay osd;

        /// <summary>
        /// Re-spawn times and enemy settings
        /// </summary>
        protected float
            playerRespawnTimer = 0f,
            playerRespawnTime = 3f,
            enemySpawnTimer = 0f,
            enemySpawnTime = 1f,
            enemySpeed,
            enemyAcceleration;

        /// <summary>
        /// Player spawn point
        /// </summary>
        protected Vector2 playerSpawnPoint;

        /// <summary>
        /// Random object for generating random numbers
        /// </summary>
        protected Random random = new Random();

        /// <summary>
        /// Initializes a new instance of the Level class
        /// </summary>
        /// <param name="playerSpawnPoint">Player spawn point</param>
        public Level(Vector2 playerSpawnPoint)
        {
            this.Initialize();
            this.playerSpawnPoint = playerSpawnPoint;
        }

        /// <summary>
        /// Initializes a new instance of the Level class
        /// </summary>
        public Level()
        {
            this.playerSpawnPoint = new Vector2(Globals.WIDTH / 2, Globals.HEIGHT / 2);
            this.Initialize();
        }

        /// <summary>
        /// Draw level elements
        /// </summary>
        /// <param name="spriteBatch">The SpriteBatch object</param>
        public void Draw(SpriteBatch spriteBatch)
        {
            EntityManager.Draw(spriteBatch);
            this.osd.Draw(spriteBatch);
        }

        /// <summary>
        /// Initialize the settings at the beginning of a new wave.
        /// </summary>
        public void Initialize()
        {
            this.enemiesToBeat = GameRules.BaseEnemiesToWin;
            this.maxEnemies = GameRules.BaseMaxEnemiesPresent;
            this.minEnemies = GameRules.BaseMinEnemiesPresent;
            this.enemySpeed = GameRules.BaseEnemySpeed;
            this.enemyAcceleration = GameRules.BaseEnemyAcceleration;
            this.playerLives = GameRules.PlayerLives;
            this.enemyCount = 0;
            this.enemySpawnTime = 3f;
            this.enemySpawnTimer = 0;
            this.playerRespawnTimer = this.playerRespawnTime;
            EntityManager.Add(new Player(this.playerSpawnPoint, MathHelper.ToRadians(0f)));
            this.osd = new OnScreenDisplay();
        }

        /// <summary>
        /// Is the level complete
        /// </summary>
        /// <returns>Value indicating whether the level is complete</returns>
        public bool IsComplete()
        {
            if (this.enemyCount == this.enemiesToBeat && EntityManager.EnemyCount == 0)
            {
                EntityManager.Clear();
                GameRules.OpenUpgradeMenu();
                this.levelComplete = true;
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Is the game over
        /// </summary>
        /// <returns>Returns a value indicating whether the game is over</returns>
        public bool IsGameOver()
        {
            if (this.playerLives <= 0 && EntityManager.PlayerDead)
            {
                EntityManager.Clear();
                GameRules.GameOver();
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Update the Level
        /// </summary>
        /// <param name="gameTime">The GameTime Object</param>
        public void Update(GameTime gameTime)
        {
            if (this.levelComplete)
            {
                this.NextLevel();
            }

            this.PlayerSpawner(gameTime);
            this.EnemySpawner(gameTime);
            this.UpdateOSD(gameTime);
            EntityManager.Update(gameTime);
            this.IsComplete();
            this.IsGameOver();
        }

        /// <summary>
        /// Spawn Enemies
        /// </summary>
        /// <param name="gameTime">The GameTime object</param>
        protected virtual void EnemySpawner(GameTime gameTime)
        {
            this.enemySpawnTimer += GameTimeHelper.GetTimeSinceLastTick(gameTime);
            if ((EntityManager.EnemyCount <= this.minEnemies
                && this.enemyCount < this.enemiesToBeat)
                || (EntityManager.EnemyCount < this.maxEnemies
                && this.enemyCount < this.enemiesToBeat
                && this.enemySpawnTimer > this.enemySpawnTime))
            {
                int width = this.random.Next(0, Globals.WIDTH);
                int height = this.random.Next(0, Globals.HEIGHT);
                Enemy enemy = new Enemy(new Vector2(width, height), MathHelper.ToRadians(0), this.enemyAcceleration, this.enemySpeed, this.enemyDifficulty, 0.15f);
                EntityManager.Add(enemy);
                this.enemySpawnTimer = 0;
                this.enemyCount++;
            }
        }

        /// <summary>
        /// Generates settings for the next level of difficulty
        /// </summary>
        private void NextLevel()
        {
            // Increase is by how much the variable increases, Rate is how many levels inbetween each increase
            this.Initialize();
            int enemyIncrease = 5;
            int enemyIncreaseRate = 1;
            int maxEnemyIncrease = 1;
            int maxEnemyIncreaseRate = 1;
            int minEnemyIncrease = 1;
            int minEnemyIncreaseRate = 2;
            float enemySpawnTimeDecrease = 0.1f;
            int enemySpawnTimeDecreaseRate = 1;
            float enemySpeedIncrease = 0.2f;
            int enemySpeedIncreaseRate = 2;
            float enemyAccelerationIncrease = 0.2f;
            int enemyAccelerationIncreaseRate = 4;

            for (int i = 1; i <= GameRules.LevelCount; i++)
            {
                if (i % enemyIncreaseRate == 0)
                {
                    this.enemiesToBeat += enemyIncrease;
                }

                if (i % maxEnemyIncreaseRate == 0)
                {
                    this.maxEnemies += maxEnemyIncrease;
                }

                if (i % minEnemyIncreaseRate == 0)
                {
                    this.minEnemies += minEnemyIncrease;
                }

                if (i % enemySpeedIncreaseRate == 0)
                {
                    this.enemySpeed += enemySpeedIncrease;
                }

                if (i % enemyAccelerationIncreaseRate == 0)
                {
                    this.enemyAcceleration += enemyAccelerationIncrease;
                }

                if (i % enemySpawnTimeDecreaseRate == 0)
                {
                    this.enemySpawnTime -= enemySpawnTimeDecrease;
                    if (this.enemySpawnTime < 0)
                    {
                        this.enemySpawnTime = 0;
                    }
                }

                if (i > 5)
                {
                    this.enemyDifficulty = 2;
                }
            }

            this.playerRespawnTimer = 0;

            this.levelComplete = false;
        }

        /// <summary>
        /// Spawn Enemies
        /// </summary>
        /// <param name="gameTime">The GameTime object</param>
        private void PlayerSpawner(GameTime gameTime)
        {
            if (EntityManager.PlayerDead)
            {
                this.playerRespawnTimer += GameTimeHelper.GetTimeSinceLastTick(gameTime);
                if (this.playerRespawnTimer > this.playerRespawnTime)
                {
                    Player newPlayer = new Player(new Vector2(Globals.WIDTH / 2, Globals.HEIGHT / 2), MathHelper.ToRadians(0f));
                    while (EntityManager.IsWithinProximity(newPlayer, 50))
                    {
                        int width = this.random.Next(0, Globals.WIDTH);
                        int height = this.random.Next(0, Globals.HEIGHT);
                        newPlayer = new Player(new Vector2(width, height), MathHelper.ToRadians(0f));
                    }

                    EntityManager.Add(newPlayer);
                    this.playerLives--;
                    this.playerRespawnTimer = 0;
                }
            }
        }

        /// <summary>
        /// Update the on screen display
        /// </summary>
        /// <param name="gameTime">The GameTime object</param>
        private void UpdateOSD(GameTime gameTime)
        {
            this.osd.Update(gameTime);
        }
    }
}