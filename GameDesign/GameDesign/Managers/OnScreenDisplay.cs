﻿using GameDesign.Content;
using GameDesign.Helpers;
using GameDesign.Rules;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace GameDesign.Managers
{
    /// <summary>
    /// The On Screen Display
    /// </summary>
    internal class OnScreenDisplay
    {
        /// <summary>
        /// Aim arrow texture
        /// </summary>
        private Texture2D aimArrow;

        /// <summary>
        /// Health Bar texture
        /// </summary>
        private Texture2D healthBar;

        /// <summary>
        /// Initializes a new instance of the OnScreenDisplay class
        /// </summary>
        public OnScreenDisplay()
        {
            this.LoadContent();
        }

        /// <summary>
        /// Draw the on screen display
        /// </summary>
        /// <param name="spriteBatch">the SpriteBatch object</param>
        public void Draw(SpriteBatch spriteBatch)
        {
            if (!EntityManager.PlayerDead)
            {
                this.DrawHealthBar(spriteBatch);
                this.DrawCrossHair(spriteBatch);
                this.DrawWeaponChoice(spriteBatch);
            }

            this.DrawPoints(spriteBatch);
        }

        /// <summary>
        /// Update the on screen display
        /// </summary>
        /// <param name="gameTime">The GameTime object</param>
        public void Update(GameTime gameTime)
        {
        }

        /// <summary>
        /// Draw the crosshair
        /// </summary>
        /// <param name="spriteBatch">The SpriteBatch object</param>
        private void DrawCrossHair(SpriteBatch spriteBatch)
        {
            MouseState mouseState = Mouse.GetState();
            Vector2 mousePosition = new Vector2(mouseState.X, mouseState.Y);
            Vector2 aimVector = mousePosition - EntityManager.Player1.Position;
            float angle = AngleHelper.VectorToRadians(aimVector);

            spriteBatch.Begin();
            spriteBatch.Draw(
                this.aimArrow,
                mousePosition,
                new Rectangle(0, 0, this.aimArrow.Width, this.aimArrow.Height),
                Color.White,
                angle,
                new Vector2(this.aimArrow.Width / 2, this.aimArrow.Height / 2),
                1f,
                SpriteEffects.None,
                0);
            spriteBatch.End();
        }

        /// <summary>
        /// Draw the health bar
        /// </summary>
        /// <param name="spriteBatch">The SpriteBatch object</param>
        private void DrawHealthBar(SpriteBatch spriteBatch)
        {
            spriteBatch.Begin(SpriteSortMode.Deferred, null, SamplerState.LinearWrap, null, null);
            spriteBatch.Draw(
                this.healthBar,
                new Vector2(0, Globals.HEIGHT - (this.healthBar.Height / 2)),
                new Rectangle(0, 0, (int)(Globals.WIDTH * EntityManager.Player1.HealthPercent), this.healthBar.Height),
                Color.Green,
                0,
                Vector2.Zero,
                1f,
                SpriteEffects.None,
                0);
            spriteBatch.End();
        }

        /// <summary>
        /// Draw the points
        /// </summary>
        /// <param name="spriteBatch">The SpriteBatch object</param>
        private void DrawPoints(SpriteBatch spriteBatch)
        {
            SpriteFont font = LoadedContent.GetFont("Title");
            spriteBatch.Begin();
            spriteBatch.DrawString(font, GameRules.GetTotalPoints().ToString(), new Vector2(10, 10), Color.Green);
            spriteBatch.End();
        }

        /// <summary>
        /// Draw the weapon choice
        /// </summary>
        /// <param name="spriteBatch">The SpriteBatch object</param>
        private void DrawWeaponChoice(SpriteBatch spriteBatch)
        {
            Texture2D weapon = EntityManager.Player1.GUIWeaponImage;

            spriteBatch.Begin();
            spriteBatch.Draw(
                weapon,
                new Vector2(Globals.WIDTH - weapon.Width - 10, 10),
                new Rectangle(0, 0, weapon.Width, weapon.Height),
                Color.White,
                0,
                Vector2.Zero,
                1f,
                SpriteEffects.None,
                0);
            spriteBatch.End();
        }

        /// <summary>
        /// Load content needed for the on screen display
        /// </summary>
        private void LoadContent()
        {
            this.healthBar = LoadedContent.GetTexture("HealthBar");
            this.aimArrow = LoadedContent.GetTexture("AimArrow");
        }
    }
}