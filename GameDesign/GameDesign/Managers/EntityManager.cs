﻿using System.Collections.Generic;
using System.Linq;
using GameDesign.Models;
using GameDesign.Models.Powerups;
using GameDesign.Models.Projectiles;
using GameDesign.Rules;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameDesign.Managers
{
    /// <summary>
    /// Manager for all entities in play
    /// </summary>
    internal static class EntityManager
    {
        /// <summary>
        /// List of active power ups
        /// </summary>
        private static List<Powerup> activePowerups = new List<Powerup>();

        /// <summary>
        /// List of sprites added this update tick
        /// </summary>
        private static List<Sprite> addedSprites = new List<Sprite>();

        /// <summary>
        /// List of enemies
        /// </summary>
        private static List<Enemy> enemies = new List<Enemy>();

        /// <summary>
        /// Is the entity manager updating
        /// </summary>
        private static bool isUpdating;

        /// <summary>
        /// List of particles
        /// </summary>
        private static List<Particle> particles = new List<Particle>();

        /// <summary>
        /// List of players
        /// </summary>
        private static List<Player> player = new List<Player>();

        /// <summary>
        /// List of power ups
        /// </summary>
        private static List<Powerup> powerups = new List<Powerup>();

        /// <summary>
        /// List of projectiles
        /// </summary>
        private static List<Projectile> projectiles = new List<Projectile>();

        /// <summary>
        /// Gets the value of the number of enemies in play
        /// </summary>
        public static int EnemyCount
        {
            get { return enemies.Count; }
        }

        /// <summary>
        /// Gets the player
        /// </summary>
        public static Player Player1
        {
            get { return player.FirstOrDefault(); }
        }

        /// <summary>
        /// Gets a value indicating whether the player is dead
        /// </summary>
        public static bool PlayerDead
        {
            get { return player.Count == 0; }
        }

        /// <summary>
        /// Add sprite to the entity manager
        /// </summary>
        /// <param name="sprite">Sprite to add</param>
        public static void Add(Sprite sprite)
        {
            if (!isUpdating)
            {
                AddSprite(sprite);
            }
            else
            {
                addedSprites.Add(sprite);
            }
        }

        /// <summary>
        /// Clear the entity manager of all sprites
        /// </summary>
        public static void Clear()
        {
            enemies.ForEach(x => x.InstantDelete());
            enemies.Clear();
            player.ForEach(x => x.InstantDelete());
            player.Clear();
            particles.ForEach(x => x.InstantDelete());
            particles.Clear();
            projectiles.ForEach(x => x.InstantDelete());
            projectiles.Clear();
            powerups.ForEach(x => x.InstantDelete());
            powerups.Clear();
            activePowerups.ForEach(x => x.InstantDelete());
            activePowerups.Clear();
        }

        /// <summary>
        /// Draw all sprites in the Entity Manager
        /// </summary>
        /// <param name="spriteBatch">The SpriteBatch object</param>
        public static void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Begin();
            particles.ForEach(x => x.Draw(spriteBatch));
            powerups.ForEach(x => x.Draw(spriteBatch));
            projectiles.ForEach(x => x.Draw(spriteBatch));
            enemies.ForEach(x => x.Draw(spriteBatch));
            if (!PlayerDead)
            {
                Player1.Draw(spriteBatch);
            }

            spriteBatch.End();
        }

        /// <summary>
        /// Returns whether a player is within a certain range of an enemy
        /// </summary>
        /// <param name="player">  Player sprite</param>
        /// <param name="distance">Distance to check</param>
        /// <returns>A value indicating whether the player is too close</returns>
        public static bool IsWithinProximity(Player player, int distance)
        {
            foreach (Enemy enemy in enemies)
            {
                float radius = player.Radius + enemy.Radius + distance;
                bool isTooClose = Vector2.Distance(player.Position, enemy.Position) < radius;
                if (isTooClose)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Pause sounds when the game is paused
        /// </summary>
        public static void Pause()
        {
            enemies.ForEach(x => x.StopNoise());
            player.ForEach(x => x.StopNoise());
        }

        /// <summary>
        /// Update all sprites in the Entity Manager
        /// </summary>
        /// <param name="gameTime">The GameTime object</param>
        public static void Update(GameTime gameTime)
        {
            isUpdating = true;
            ActivatePowerups();
            ApplyPowerups();
            CheckCollisions();
            DeleteSprites();
            UpdateSprites(gameTime);
            isUpdating = false;

            addedSprites.ForEach(sprite => AddSprite(sprite));
            addedSprites.Clear();
        }

        /// <summary>
        /// Activate collected power ups
        /// </summary>
        private static void ActivatePowerups()
        {
            if (!PlayerDead)
            {
                List<Powerup> activate = new List<Powerup>();
                foreach (Powerup powerup in powerups)
                {
                    if (IsColliding(powerup, Player1))
                    {
                        activate.Add(powerup);
                    }
                }

                foreach (Powerup powerup in activate)
                {
                    activePowerups.Add(powerup);
                    powerups.Remove(powerup);
                }
            }
        }

        /// <summary>
        /// Add a sprite to the Entity Manager
        /// </summary>
        /// <param name="sprite">The Sprite to be added</param>
        private static void AddSprite(Sprite sprite)
        {
            if (sprite is Projectile)
            {
                projectiles.Add(sprite as Projectile);
            }

            if (sprite is Player)
            {
                player.Add(sprite as Player);
            }

            if (sprite is Enemy)
            {
                enemies.Add(sprite as Enemy);
            }

            if (sprite is Particle)
            {
                particles.Add(sprite as Particle);
            }

            if (sprite is Powerup)
            {
                powerups.Add(sprite as Powerup);
            }
        }

        /// <summary>
        /// Apply all active power ups
        /// </summary>
        private static void ApplyPowerups()
        {
            if (!PlayerDead)
            {
                activePowerups.ForEach(powerup => powerup.Apply(Player1));
            }
        }

        /// <summary>
        /// Check for Collisions between Sprites and deal any relevant damage
        /// </summary>
        private static void CheckCollisions()
        {
            foreach (Projectile projectile in projectiles)
            {
                foreach (Enemy enemy in enemies)
                {
                    if (IsColliding(enemy, projectile))
                    {
                        DealDamage(enemy, projectile);
                    }
                }
            }

            if (!PlayerDead)
            {
                foreach (Enemy enemy in enemies)
                {
                    if (IsColliding(enemy, Player1))
                    {
                        DealDamage(enemy, Player1);
                    }
                }
            }
        }

        /// <summary>
        /// DealDamage between two sprites
        /// </summary>
        /// <param name="a">Sprite A</param>
        /// <param name="b">Sprite B</param>
        private static void DealDamage(Sprite a, Sprite b)
        {
            a.TakeDamage(b);
            b.TakeDamage(a);
        }

        /// <summary>
        /// Delete all sprites that have been marked deleted
        /// </summary>
        private static void DeleteSprites()
        {
            // Delete Sprites
            projectiles = projectiles.Where(x => !x.IsDeleted).ToList();
            particles = particles.Where(x => !x.IsDeleted).ToList();
            enemies = enemies.Where(x => !x.IsDeleted).ToList();
            powerups = powerups.Where(x => !x.IsDeleted).ToList();
            activePowerups = activePowerups.Where(x => !x.IsDeleted).ToList();
            player = player.Where(x => !x.IsDeleted).ToList();

            // Purge Sprites from outside screen boundaries
            PurgeProjectiles();
            PurgeParticles();
        }

        /// <summary>
        /// Returns a value indicating whether two sprites are colliding.
        /// </summary>
        /// <param name="a">Sprite A</param>
        /// <param name="b">Sprite B</param>
        /// <returns>Value indicating whether the sprites are colliding</returns>
        private static bool IsColliding(Sprite a, Sprite b)
        {
            float radius = a.Radius + b.Radius;
            return !a.IsDead && !b.IsDead && Vector2.Distance(a.Position, b.Position) < radius;
        }

        /// <summary>
        /// Purge Particles from memory when they leave the screen
        /// </summary>
        private static void PurgeParticles()
        {
            List<Particle> toDelete = particles.Where(x =>
                x.Position.X < 0 ||
                x.Position.Y < 0 ||
                x.Position.X > Globals.WIDTH ||
                x.Position.Y > Globals.HEIGHT).ToList();

            toDelete.ForEach(projectile => particles.Remove(projectile));
        }

        /// <summary>
        /// Purge projectiles from memory when they leave the screen
        /// </summary>
        private static void PurgeProjectiles()
        {
            List<Projectile> toDelete = projectiles.Where(x =>
                x.Position.X < 0 ||
                x.Position.Y < 0 ||
                x.Position.X > Globals.WIDTH ||
                x.Position.Y > Globals.HEIGHT).ToList();

            toDelete.ForEach(projectile => projectiles.Remove(projectile));
        }

        /// <summary>
        /// Update sprites
        /// </summary>
        /// <param name="gameTime">The GameTime object</param>
        private static void UpdateSprites(GameTime gameTime)
        {
            enemies.ForEach(x => x.Update(gameTime));
            projectiles.ForEach(x => x.Update(gameTime));
            particles.ForEach(x => x.Update(gameTime));
            powerups.ForEach(x => x.Update(gameTime));
            activePowerups.ForEach(x => x.Update(gameTime));

            if (!PlayerDead)
            {
                Player1.Update(gameTime);
            }
        }
    }
}