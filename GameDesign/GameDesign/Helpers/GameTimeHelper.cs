﻿using GameDesign.Rules;
using Microsoft.Xna.Framework;

namespace GameDesign.Helpers
{
    /// <summary>
    /// Helper for GameTime related calculations
    /// </summary>
    public static class GameTimeHelper
    {
        /// <summary>
        /// Get a float value indicating the number of seconds since the game started
        /// </summary>
        /// <param name="gameTime">The GameTime object</param>
        /// <returns>Value indicating the number of seconds since the game started</returns>
        public static float GetCurrentGameTime(GameTime gameTime)
        {
            return (float)gameTime.TotalGameTime.TotalMilliseconds * Globals.MILLICONST;
        }

        /// <summary>
        /// Get a float value indicating the number of seconds since the last tick
        /// </summary>
        /// <param name="gameTime">The GameTime object</param>
        /// <returns>Value indicating the number of seconds since the last tick</returns>
        public static float GetTimeSinceLastTick(GameTime gameTime)
        {
            return (float)gameTime.ElapsedGameTime.TotalMilliseconds * Globals.MILLICONST;
        }
    }
}