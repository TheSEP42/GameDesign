﻿namespace GameDesign.Helpers
{
    /// <summary>
    /// Text alignment
    /// </summary>
    public enum Alignment
    {
        /// <summary>
        /// Align centered
        /// </summary>
        Center = 0,

        /// <summary>
        /// Align left
        /// </summary>
        Left = 1,

        /// <summary>
        /// Align right
        /// </summary>
        Right = 2,

        /// <summary>
        /// Align top
        /// </summary>
        Top = 3,

        /// <summary>
        /// Align bottom
        /// </summary>
        Bottom = 4
    }
}