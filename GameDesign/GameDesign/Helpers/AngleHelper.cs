﻿using System;
using Microsoft.Xna.Framework;

namespace GameDesign.Helpers
{
    /// <summary>
    /// Helper for angle related calculations
    /// </summary>
    public static class AngleHelper
    {
        /// <summary>
        /// Convert Degrees to a vector
        /// </summary>
        /// <param name="degrees">Angle in degrees</param>
        /// <returns>Direction vector</returns>
        public static Vector2 DegreesToVector(float degrees)
        {
            float radians = MathHelper.ToRadians(degrees);
            return new Vector2(
                (float)Math.Cos(radians),
                -(float)Math.Sin(radians));
        }

        /// <summary>
        /// Convert Radians to a vector
        /// </summary>
        /// <param name="radians">Angle in radians</param>
        /// <returns>Direction vector</returns>
        public static Vector2 RadiansToVector(float radians)
        {
            return new Vector2(
                (float)Math.Cos(radians),
                -(float)Math.Sin(radians));
        }

        /// <summary>
        /// Convert a direction vector to degrees
        /// </summary>
        /// <param name="vector">Direction vector</param>
        /// <returns>Angle in degrees</returns>
        public static float VectorToDegrees(Vector2 vector)
        {
            return MathHelper.ToDegrees((float)Math.Atan2(vector.X, -vector.Y));
        }

        /// <summary>
        /// Convert a direction vector to radians
        /// </summary>
        /// <param name="vector">Direction vector</param>
        /// <returns>Angle in radians</returns>
        public static float VectorToRadians(Vector2 vector)
        {
            return (float)Math.Atan2(vector.X, -vector.Y);
        }
    }
}