﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameDesign.Helpers
{
    /// <summary>
    /// Helper for drawing strings
    /// </summary>
    internal class StringHelper
    {
        /// <summary>
        /// Draw the string to the screen
        /// </summary>
        /// <param name="spriteBatch">The SpriteBatch object</param>
        /// <param name="font">       The font</param>
        /// <param name="text">       The text</param>
        /// <param name="bounds">     The bounds to fit the text</param>
        /// <param name="align">      The text alignment</param>
        /// <param name="color">      The color</param>
        public static void DrawString(SpriteBatch spriteBatch, SpriteFont font, string text, Rectangle bounds, Alignment align, Color color)
        {
            Vector2 size = font.MeasureString(text);
            Vector2 pos = GetCenter(bounds);
            Vector2 origin = size * 0.5f;

            if (align == Alignment.Left)
            {
                origin.X += (bounds.Width / 2) - (size.X / 2);
            }

            if (align == Alignment.Right)
            {
                origin.X -= (bounds.Width / 2) - (size.X / 2);
            }

            if (align == Alignment.Top)
            {
                origin.Y += (bounds.Height / 2) - (size.Y / 2);
            }

            if (align == Alignment.Bottom)
            {
                origin.Y -= (bounds.Height / 2) - (size.Y / 2);
            }

            spriteBatch.DrawString(font, text, pos, color, 0, origin, 1, SpriteEffects.None, 0);
        }

        /// <summary>
        /// Get the center of a rectangle
        /// </summary>
        /// <param name="rect">The rectangle</param>
        /// <returns>A vector indicating the center of the rectangle</returns>
        private static Vector2 GetCenter(Rectangle rect)
        {
            return new Vector2(
                rect.Left + (rect.Width / 2),
                rect.Top + (rect.Height / 2));
        }
    }
}