﻿using System;
using System.Collections.Generic;
using System.Linq;
using GameDesign.Managers;
using GameDesign.Models.Powerups;
using GameDesign.Models.Weapons;

namespace GameDesign.Rules
{
    /// <summary>
    /// Static class of game rules
    /// </summary>
    internal static class GameRules
    {
        /// <summary>
        /// base enemies to beat a level
        /// </summary>
        public const int BaseEnemiesToWin = 10;

        /// <summary>
        /// Base enemy acceleration
        /// </summary>
        public const float BaseEnemyAcceleration = 5.0f;

        /// <summary>
        /// Base enemy speed
        /// </summary>
        public const float BaseEnemySpeed = 3.0f;

        /// <summary>
        /// Base max enemies allowed on screen
        /// </summary>
        public const int BaseMaxEnemiesPresent = 6;

        /// <summary>
        /// Base minimum enemies allowed on screen
        /// </summary>
        public const int BaseMinEnemiesPresent = 3;

        /// <summary>
        /// Player health
        /// </summary>
        public const int PlayerHealth = 1000;

        /// <summary>
        /// Player lives
        /// </summary>
        public const int PlayerLives = 3;

        /// <summary>
        /// Percentage of weapon drops
        /// </summary>
        private const float PowerupDropPercentage = 0.33f;

        /// <summary>
        /// Base levels for power ups
        /// </summary>
        private static readonly Dictionary<PowerupType, int> BasePowerupLevels = new Dictionary<PowerupType, int>()
        {
            { PowerupType.Health, 0 },
            { PowerupType.Shield, 0 }
        };

        /// <summary>
        /// Base levels for weapons
        /// </summary>
        private static readonly Dictionary<WeaponType, int> BaseWeaponLevels = new Dictionary<WeaponType, int>()
        {
            { WeaponType.BasicGun,  0 },
            { WeaponType.RocketGun, 0 },
            { WeaponType.DoubleGun, 0 }
        };

        /// <summary>
        /// Weighting of power up drop likelihood
        /// </summary>
        private static Dictionary<PowerupType, int> dropWeighting = new Dictionary<PowerupType, int>()
        {
            { PowerupType.Health, 1 },
            { PowerupType.Shield, 1 }
        };

        /// <summary>
        /// List of player weapons
        /// </summary>
        private static Dictionary<WeaponType, Weapon> playerWeapons = new Dictionary<WeaponType, Weapon>()
        {
            { WeaponType.BasicGun,  new BasicGun(8, 15, 10) },
            { WeaponType.RocketGun, new RocketGun(9, 2, 50, 5) },
            { WeaponType.DoubleGun, new DoubleGun(15, 15, 8) }
        };

        /// <summary>
        /// Lost of power up levels
        /// </summary>
        private static Dictionary<PowerupType, int> powerupLevels = new Dictionary<PowerupType, int>()
        {
            { PowerupType.Health, 0 },
            { PowerupType.Shield, 0 }
        };

        /// <summary>
        /// Gets a value indicating whether the game should reload levels and bosses (at a new game)
        /// </summary>
        private static bool ReLoad = false;

        /// <summary>
        /// Total collected points
        /// </summary>
        private static int totalPoints = 0;

        /// <summary>
        /// List of weapon levels
        /// </summary>
        private static Dictionary<WeaponType, int> weaponLevels = new Dictionary<WeaponType, int>()
        {
            { WeaponType.BasicGun,  0 },
            { WeaponType.RocketGun, 0 },
            { WeaponType.DoubleGun, 0 }
        };

        /// <summary>
        /// Gets the current level count
        /// </summary>
        public static int LevelCount { get; private set; } = 1;

        /// <summary>
        /// Gets the state when paused
        /// </summary>
        public static GameState? PauseState { get; private set; }

        /// <summary>
        /// Gets the state when quit to main menu
        /// </summary>
        public static GameState? QuitToMenuState { get; private set; }

        /// <summary>
        /// Gets the game state
        /// </summary>
        public static GameState State { get; private set; } = GameState.MainMenu;

        /// <summary>
        /// Add points to the total
        /// </summary>
        /// <param name="points">Points to add</param>
        public static void AddPoints(int points)
        {
            totalPoints += points;
        }

        /// <summary>
        /// Enter back story state
        /// </summary>
        public static void BackStory()
        {
            State = GameState.BackStory;
        }

        /// <summary>
        /// Enter controls state
        /// </summary>
        public static void Controls()
        {
            State = GameState.Controls;
        }

        /// <summary>
        /// Enter game complete state
        /// </summary>
        public static void GameComplete()
        {
            State = GameState.GameComplete;
        }

        /// <summary>
        /// Enter game over state
        /// </summary>
        public static void GameOver()
        {
            State = GameState.GameOver;
        }

        /// <summary>
        /// Randomly decide on the power up type to use
        /// </summary>
        /// <returns>The Power up type</returns>
        public static PowerupType? GeneratePowerupType()
        {
            List<PowerupType> types = Enum.GetValues(typeof(PowerupType)).Cast<PowerupType>().ToList();
            Random rand = new Random();

            if (rand.NextDouble() < GameRules.PowerupDropPercentage)
            {
                int total = 0;
                foreach (PowerupType type in types)
                {
                    total += dropWeighting[type];
                }

                int choice = rand.Next(1, total + 1);
                int choiceCounter = 0;

                foreach (PowerupType type in types)
                {
                    choiceCounter += dropWeighting[type];
                    if (choice <= choiceCounter)
                    {
                        return type;
                    }
                }

                return null;
            }

            return null;
        }

        /// <summary>
        /// Get player weapon
        /// </summary>
        /// <param name="weapon">Weapon type</param>
        /// <returns>Requested weapon</returns>
        public static Weapon GetPlayerWeapon(WeaponType weapon)
        {
            return playerWeapons[weapon];
        }

        /// <summary>
        /// Returns total points for drawing to screen and affordability calculations
        /// </summary>
        /// <returns>Total points</returns>
        public static int GetTotalPoints()
        {
            return totalPoints;
        }

        /// <summary>
        /// Enter main menu state
        /// </summary>
        public static void MainMenu()
        {
            if (State != GameState.Controls && State != GameState.BackStory)
            {
                QuitToMenuState = State;
            }

            State = GameState.MainMenu;
        }

        /// <summary>
        /// Returns a value indicating whether the game needs to be reloaded
        /// </summary>
        /// <returns>Returns a value indicating whether the game needs to be reloaded</returns>
        public static bool NeedReloading()
        {
            return ReLoad;
        }

        /// <summary>
        /// Enter new game state
        /// </summary>
        public static void NewGame()
        {
            State = GameState.Playing;
            EntityManager.Clear();

            ReLoad = true;
            LevelCount = 1;
            totalPoints = 0;
            powerupLevels.Clear();
            weaponLevels.Clear();
            powerupLevels = CloneDictionaryCloningValues<PowerupType, int>(BasePowerupLevels);
            weaponLevels = CloneDictionaryCloningValues<WeaponType, int>(BaseWeaponLevels);
        }

        /// <summary>
        /// Start the next level
        /// </summary>
        public static void NextLevel()
        {
            State = GameState.Playing;
            LevelCount++;
        }

        /// <summary>
        /// Enter upgrade menu state
        /// </summary>
        public static void OpenUpgradeMenu()
        {
            State = GameState.UpgradeMenu;
        }

        /// <summary>
        /// Enter pause state
        /// </summary>
        public static void Pause()
        {
            if (State == GameState.Playing || State == GameState.BossLevel)
            {
                PauseState = State;
                State = GameState.Paused;
                EntityManager.Pause();
            }
        }

        /// <summary>
        /// Returns the upgrade level for a weapon type
        /// </summary>
        /// <param name="powerup">The power up for requested upgrade level</param>
        /// <returns>Value indicating the power up upgrade level</returns>
        public static int PowerupUpgradeLevel(PowerupType powerup)
        {
            int toRet = powerupLevels[powerup];
            return toRet;
        }

        /// <summary>
        /// Enter quit state
        /// </summary>
        public static void Quit()
        {
            State = GameState.Quit;
        }

        /// <summary>
        /// All assets have been reloaded
        /// </summary>
        public static void ReLoaded()
        {
            ReLoad = false;
        }

        /// <summary>
        /// Resume from previous state
        /// </summary>
        public static void Resume()
        {
            if (PauseState.HasValue)
            {
                State = PauseState.Value;
            }
            else
            {
                State = GameState.Playing;
            }
        }

        /// <summary>
        /// Spend points
        /// </summary>
        /// <param name="points">Points to spend</param>
        public static void SpendPoints(int points)
        {
            totalPoints -= points;
        }

        /// <summary>
        /// Enter boss level state
        /// </summary>
        public static void StartBossLevel()
        {
            State = GameState.BossLevel;
        }

        /// <summary>
        /// Upgrades the provided power up by one level
        /// </summary>
        /// <param name="powerup">The power up to upgrade</param>
        public static void UpgradePowerup(PowerupType powerup)
        {
            powerupLevels[powerup] += 1;
        }

        /// <summary>
        /// Upgrades the provided weapon by one level
        /// </summary>
        /// <param name="weapon">The weapon to upgrade</param>
        public static void UpgradeWeapon(WeaponType weapon)
        {
            weaponLevels[weapon] += 1;
        }

        /// <summary>
        /// Returns the upgrade level for a weapon type
        /// </summary>
        /// <param name="weapon">The weapon for requested upgrade level</param>
        /// <returns>Value indicating the weapon upgrade level</returns>
        public static int WeaponUpgradeLevel(WeaponType weapon)
        {
            int toRet = weaponLevels[weapon];
            return toRet;
        }

        /// <summary>
        /// Clone the upgrade dictionaries
        /// </summary>
        /// <typeparam name="TKey">Key type</typeparam>
        /// <typeparam name="TValue">Value type</typeparam>
        /// <param name="original">Dictionary to clone</param>
        /// <returns>Cloned dictionary</returns>
        private static Dictionary<TKey, TValue> CloneDictionaryCloningValues<TKey, TValue>(Dictionary<TKey, TValue> original)
        {
            Dictionary<TKey, TValue> ret = new Dictionary<TKey, TValue>(original.Count, original.Comparer);
            foreach (KeyValuePair<TKey, TValue> entry in original)
            {
                ret.Add(entry.Key, (TValue)entry.Value);
            }

            return ret;
        }
    }
}