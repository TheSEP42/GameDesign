﻿namespace GameDesign.Rules
{
    /// <summary>
    /// List of global game variables
    /// </summary>
    internal static class Globals
    {
        /// <summary>
        /// Screen height
        /// </summary>
        public const int HEIGHT = 720;

        /// <summary>
        /// Millisecond constant
        /// </summary>
        public const float MILLICONST = 0.001f;

        /// <summary>
        /// Screen width
        /// </summary>
        public const int WIDTH = 1280;
    }
}