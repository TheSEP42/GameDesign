﻿namespace GameDesign.Rules
{
    /// <summary>
    /// States of play
    /// </summary>
    public enum GameState
    {
        /// <summary>
        /// Playing the wave levels
        /// </summary>
        Playing,

        /// <summary>
        /// Paused in game
        /// </summary>
        Paused,

        /// <summary>
        /// Upgrade menu
        /// </summary>
        UpgradeMenu,

        /// <summary>
        /// Playing the boss level
        /// </summary>
        BossLevel,

        /// <summary>
        /// Game complete
        /// </summary>
        GameComplete,

        /// <summary>
        /// Game over
        /// </summary>
        GameOver,

        /// <summary>
        /// Main menu
        /// </summary>
        MainMenu,

        /// <summary>
        /// Controls screen
        /// </summary>
        Controls,

        /// <summary>
        /// Back story screen
        /// </summary>
        BackStory,

        /// <summary>
        /// Quit the game
        /// </summary>
        Quit
    }
}