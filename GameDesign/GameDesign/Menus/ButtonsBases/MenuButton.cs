﻿using System;
using GameDesign.Helpers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace GameDesign.Menus.ButtonBases
{
    /// <summary>
    /// Button for menus
    /// </summary>
    public abstract class MenuButton
    {
        /// <summary>
        /// The base scale of the button
        /// </summary>
        protected float baseScale;

        /// <summary>
        /// The color of the button
        /// </summary>
        protected Color color;

        /// <summary>
        /// The outline used for the button
        /// </summary>
        protected Texture2D outline;

        /// <summary>
        /// The current scale of the button
        /// </summary>
        protected float scale;

        /// <summary>
        /// The percentage to change on hover and press
        /// </summary>
        protected float scalePercent;

        /// <summary>
        /// The time to animate the scale change when hovering or pressing
        /// </summary>
        protected float scaleTime = 0.1f;

        /// <summary>
        /// The angle of the button
        /// </summary>
        private float angle;

        /// <summary>
        /// The content of the button
        /// </summary>
        private Texture2D content;

        /// <summary>
        /// Has the cursor been released
        /// </summary>
        private bool initialCursorRelease = false;

        /// <summary>
        /// The position of the button
        /// </summary>
        private Vector2 position;

        /// <summary>
        /// Is the button pressed
        /// </summary>
        private bool pressed;

        /// <summary>
        /// The previous state of the button
        /// </summary>
        private ButtonState previousMouseButtonState;

        /// <summary>
        /// The outline when the button is unavailable
        /// </summary>
        private Texture2D unavailableOutline;

        /// <summary>
        /// Initializes a new instance of the MenuButton class
        /// </summary>
        /// <param name="position">Position of the button</param>
        /// <param name="outline"> Outline of the button</param>
        /// <param name="content"> Content of the button</param>
        /// <param name="color">   Color of the button</param>
        /// <param name="scale">   Scale of the button</param>
        public MenuButton(Vector2 position, Texture2D outline, Texture2D content, Color color, float scale)
        {
            this.position = position;
            this.outline = outline;

            this.content = content;
            this.color = color;
            this.scale = scale;
            this.baseScale = scale;
            this.scalePercent = 0.1f;
            this.angle = 0;
            this.previousMouseButtonState = Mouse.GetState().LeftButton;

            this.pressed = false;
        }

        /// <summary>
        /// Gets a rectangle representing the draw location of this button
        /// </summary>
        /// <returns>Rectangle of the hit box</returns>
        protected Rectangle HitBox
        {
            get
            {
                Texture2D outline = this.outline;

                Rectangle buttonRectangle = new Rectangle(
                    this.DrawRectangle(outline).X,
                    this.DrawRectangle(outline).Y,
                    Convert.ToInt32(this.DrawRectangle(outline).Width * this.scale),
                    Convert.ToInt32(this.DrawRectangle(outline).Height * this.scale));

                Vector2 positionVector = this.position - new Vector2((outline.Width * this.scale) / 2, (outline.Height * this.scale) / 2);
                buttonRectangle.Location = new Point(Convert.ToInt32(positionVector.X), Convert.ToInt32(positionVector.Y));
                return buttonRectangle;
            }
        }

        /// <summary>
        /// Gets a Point indicating the location of the mouse
        /// </summary>
        /// <returns>The point of the mouse</returns>
        private Point MousePosition
        {
            get
            {
                MouseState mouseState = Mouse.GetState();
                return new Point(Convert.ToInt32(mouseState.X), Convert.ToInt32(mouseState.Y));
            }
        }

        /// <summary>
        /// Draw the button
        /// </summary>
        /// <param name="spriteBatch">The SpriteBatch Object</param>
        public virtual void Draw(SpriteBatch spriteBatch)
        {
            // Draw content
            spriteBatch.Draw(
                this.content,
                this.position,
                this.DrawRectangle(this.content),
                this.color,
                this.angle,
                this.Origin(this.content),
                this.scale,
                SpriteEffects.None,
                0f);

            // Draw outline
            spriteBatch.Draw(
                this.outline,
                this.position,
                this.DrawRectangle(this.outline),
                this.color,
                this.angle,
                this.Origin(this.outline),
                this.scale,
                SpriteEffects.None,
                0f);
        }

        /// <summary>
        /// Update this button
        /// </summary>
        /// <param name="gameTime">The GameTime object</param>
        public virtual void Update(GameTime gameTime)
        {
            this.CheckInitialCursorRelease();
            if (this.initialCursorRelease)
            {
                this.CheckHover(gameTime);
                this.CheckPress(gameTime);
            }
        }

        /// <summary>
        /// Check to see if the mouse is over this button
        /// </summary>
        /// <param name="gameTime">The GameTime object</param>
        protected virtual void CheckHover(GameTime gameTime)
        {
            if (this.MouseOver())
            {
                this.Lift(gameTime);
            }
            else
            {
                this.Release(gameTime);
            }
        }

        /// <summary>
        /// Checks to see if the button is pressed
        /// </summary>
        /// <param name="gameTime">The GameTime object</param>
        protected virtual void CheckPress(GameTime gameTime)
        {
            if (this.MouseOver())
            {
                if (Mouse.GetState().LeftButton == ButtonState.Pressed)
                {
                    this.pressed = true;
                    this.Depress(gameTime);
                }
                else if (this.pressed)
                {
                    this.Press();
                    this.pressed = false;
                }
            }
            else
            {
                this.pressed = false;
            }
        }

        /// <summary>
        /// Depresses the button
        /// </summary>
        /// <param name="gameTime">The GameTime object</param>
        protected void Depress(GameTime gameTime)
        {
            this.ScaleButton(gameTime, this.baseScale - (this.baseScale * this.scalePercent));
        }

        /// <summary>
        /// Lifts the button
        /// </summary>
        /// <param name="gameTime">The GameTime object</param>
        protected void Lift(GameTime gameTime)
        {
            this.ScaleButton(gameTime, this.baseScale + (this.baseScale * this.scalePercent));
        }

        /// <summary>
        /// Returns if the mouse is over the button
        /// </summary>
        /// <returns>Returns a boolean value</returns>
        protected bool MouseOver()
        {
            return this.HitBox.Contains(this.MousePosition);
        }

        /// <summary>
        /// Press this button
        /// </summary>
        protected abstract void Press();

        /// <summary>
        /// Releases the button
        /// </summary>
        /// <param name="gameTime">The GameTime object</param>
        protected void Release(GameTime gameTime)
        {
            this.ScaleButton(gameTime, this.baseScale);
        }

        /// <summary>
        /// Check for initial cursor release
        /// </summary>
        private void CheckInitialCursorRelease()
        {
            if (Mouse.GetState().LeftButton == ButtonState.Released)
            {
                this.initialCursorRelease = true;
            }
        }

        /// <summary>
        /// Returns the draw rectangle of the button
        /// </summary>
        /// <param name="texture">Texture for calculation</param>
        /// <returns>Rectangle representing the area of the button</returns>
        private Rectangle DrawRectangle(Texture2D texture)
        {
            return new Rectangle(0, 0, texture.Width, texture.Height);
        }

        /// <summary>
        /// Returns the center of the texture
        /// </summary>
        /// <param name="texture">The texture to calculate center of</param>
        /// <returns>Vector representing the texture center</returns>
        private Vector2 Origin(Texture2D texture)
        {
            return new Vector2(texture.Width / 2, texture.Height / 2);
        }

        /// <summary>
        /// Scale the button to a certain value over time
        /// </summary>
        /// <param name="gameTime"> The GameTime object</param>
        /// <param name="goalScale">The scale to reach</param>
        private void ScaleButton(GameTime gameTime, float goalScale)
        {
            float time = GameTimeHelper.GetTimeSinceLastTick(gameTime);
            float currentScale = this.scale;
            float scaleChange = goalScale - currentScale;
            float changeBy = scaleChange * (time / this.scaleTime);
            this.scale += changeBy;
        }
    }
}