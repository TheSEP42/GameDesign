﻿namespace GameDesign.Menus.ButtonBases
{
    /// <summary>
    /// States of a menu button
    /// </summary>
    public enum MenuButtonState
    {
        /// <summary>
        /// The button is available
        /// </summary>
        Available = 0,

        /// <summary>
        /// The button is unavailable
        /// </summary>
        Unavailable = 1,

        /// <summary>
        /// The button has been used
        /// </summary>
        Used = 2
    }
}