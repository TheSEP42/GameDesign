﻿using System;
using GameDesign.Content;
using GameDesign.Helpers;
using GameDesign.Rules;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameDesign.Menus.ButtonBases
{
    /// <summary>
    /// Upgrade menu button
    /// </summary>
    internal abstract class UpgradeMenuButton : MenuButton
    {
        /// <summary>
        /// The active upgrade level
        /// </summary>
        protected int activeUpgradeLevel;

        /// <summary>
        /// The normal color of the button
        /// </summary>
        protected Color normalColor = Color.Green;

        /// <summary>
        /// The state of the button
        /// </summary>
        protected MenuButtonState state;

        /// <summary>
        /// The color when unavailable
        /// </summary>
        protected Color unavailableColor = Color.Red;

        /// <summary>
        /// The upgrade level
        /// </summary>
        protected int upgradeLevel;

        /// <summary>
        /// The upgrade type
        /// </summary>
        protected Enum upgradeType;

        /// <summary>
        /// The color when used
        /// </summary>
        protected Color usedColor = Color.Gray;

        /// <summary>
        /// The available outline
        /// </summary>
        private Texture2D availableOutline;

        /// <summary>
        /// The cost to use the button
        /// </summary>
        private int cost;

        /// <summary>
        /// The outline when unavailable
        /// </summary>
        private Texture2D unavailableOutline;

        /// <summary>
        /// Initializes a new instance of the UpgradeMenuButton class
        /// </summary>
        /// <param name="position">    The position of the button</param>
        /// <param name="content">     The content of the button</param>
        /// <param name="scale">       The scale of the button</param>
        /// <param name="type">        The type of upgrade</param>
        /// <param name="upgradeLevel">The upgrade level</param>
        /// <param name="cost">        The cost to use</param>
        public UpgradeMenuButton(
            Vector2 position,
            Texture2D content,
            float scale,
            Enum type,
            int upgradeLevel,
            int cost) : base(position, LoadedContent.GetTexture("ButtonOutline"), content, Color.Green, scale)
        {
            this.upgradeType = type;
            this.upgradeLevel = upgradeLevel;
            this.cost = cost;
            this.normalColor = Color.Green;
            this.usedColor = Color.Gray;
            this.unavailableColor = Color.Red;
            this.state = MenuButtonState.Available;
            this.availableOutline = LoadedContent.GetTexture("ButtonOutline");
            this.unavailableOutline = LoadedContent.GetTexture("ButtonOutlineCrossed");
        }

        /// <summary>
        /// Draw the button
        /// </summary>
        /// <param name="spriteBatch">The SpriteBatch object</param>
        public override void Draw(SpriteBatch spriteBatch)
        {
            this.outline = (this.state == MenuButtonState.Unavailable) ? this.unavailableOutline : this.availableOutline;
            base.Draw(spriteBatch);
            this.DrawCost(spriteBatch);
        }

        /// <summary>
        /// Update the button
        /// </summary>
        /// <param name="gameTime">The GameTime object</param>
        public override void Update(GameTime gameTime)
        {
            if (this.upgradeLevel < this.activeUpgradeLevel)
            {
                this.state = MenuButtonState.Used;
            }
            else if (this.upgradeLevel == this.activeUpgradeLevel)
            {
                if (GameRules.GetTotalPoints() >= this.cost)
                {
                    this.state = MenuButtonState.Available;
                }
                else
                {
                    this.state = MenuButtonState.Unavailable;
                }
            }
            else if (this.upgradeLevel > this.activeUpgradeLevel)
            {
                this.state = MenuButtonState.Unavailable;
            }

            this.UpdateColor();
            base.Update(gameTime);
        }

        /// <summary>
        /// Check if the mouse is over the button
        /// </summary>
        /// <param name="gameTime">The GameTime object</param>
        protected override void CheckHover(GameTime gameTime)
        {
            if (this.state == MenuButtonState.Available && this.MouseOver())
            {
                this.Lift(gameTime);
            }
            else
            {
                this.Release(gameTime);
            }
        }

        /// <summary>
        /// Check if the button is pressed
        /// </summary>
        /// <param name="gameTime">The GameTime object</param>
        protected override void CheckPress(GameTime gameTime)
        {
            if (this.state == MenuButtonState.Available)
            {
                base.CheckPress(gameTime);
            }
        }

        /// <summary>
        /// Press this button
        /// </summary>
        protected override void Press()
        {
            if (this.state == MenuButtonState.Available)
            {
                this.PressAction();
                GameRules.SpendPoints(this.cost);
            }
        }

        /// <summary>
        /// Action when pressed
        /// </summary>
        protected abstract void PressAction();

        /// <summary>
        /// Draw the cost of the button
        /// </summary>
        /// <param name="spriteBatch">The SpriteBatch object</param>
        private void DrawCost(SpriteBatch spriteBatch)
        {
            // Draw outline
            SpriteFont font = LoadedContent.GetFont("UpgradeCost");
            string costText = this.cost.ToString();
            Rectangle bounds = this.HitBox;
            bounds.Y += bounds.Height;
            bounds.Height = 20;
            StringHelper.DrawString(spriteBatch, font, costText, bounds, Alignment.Center, this.color);
        }

        /// <summary>
        /// Update the color of this button
        /// </summary>
        private void UpdateColor()
        {
            switch (this.state)
            {
                case MenuButtonState.Used:
                    this.color = this.usedColor;
                    break;

                case MenuButtonState.Unavailable:
                    this.color = this.unavailableColor;
                    break;

                case MenuButtonState.Available:
                    this.color = this.normalColor;
                    break;

                default:
                    this.color = this.normalColor;
                    break;
            }
        }
    }
}