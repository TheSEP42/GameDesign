﻿using System.Collections.Generic;
using GameDesign.Content;
using GameDesign.Menus.ButtonBases;
using GameDesign.Models;
using GameDesign.Rules;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace GameDesign.Menus
{
    /// <summary>
    /// Pertains to all menu related things
    /// </summary>
    public class Menu
    {
        /// <summary>
        /// List of buttons
        /// </summary>
        private List<MenuButton> buttons = new List<MenuButton>();

        /// <summary>
        /// Cursor texture
        /// </summary>
        private Texture2D cursor;

        /// <summary>
        /// List of static sprites
        /// </summary>
        private List<Sprite> staticSprites = new List<Sprite>();

        /// <summary>
        /// List of text
        /// </summary>
        private List<MenuText> text = new List<MenuText>();

        /// <summary>
        /// Initializes a new instance of the Menu class
        /// </summary>
        public Menu()
        {
            this.buttons = new List<MenuButton>();
            this.cursor = LoadedContent.GetTexture("AimArrow");
        }

        /// <summary>
        /// Add button
        /// </summary>
        /// <param name="button">Button to add</param>
        public void AddButton(MenuButton button)
        {
            this.buttons.Add(button);
        }

        /// <summary>
        /// Add static sprite
        /// </summary>
        /// <param name="sprite">Sprite to add</param>
        public void AddStaticSprites(Sprite sprite)
        {
            this.staticSprites.Add(sprite);
        }

        /// <summary>
        /// Add text
        /// </summary>
        /// <param name="text">Text to add</param>
        public void AddText(MenuText text)
        {
            this.text.Add(text);
        }

        /// <summary>
        /// Draw the menu
        /// </summary>
        /// <param name="spriteBatch">The SpriteBatch object</param>
        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Begin();
            this.staticSprites.ForEach(sprite => sprite.Draw(spriteBatch));
            this.buttons.ForEach(button => button.Draw(spriteBatch));
            this.text.ForEach(text => text.Draw(spriteBatch));
            spriteBatch.End();

            this.DrawPoints(spriteBatch);

            MouseState mouseState = Mouse.GetState();
            Vector2 mousePosition = new Vector2(mouseState.X, mouseState.Y);

            spriteBatch.Begin();
            spriteBatch.Draw(
                this.cursor,
                mousePosition,
                new Rectangle(0, 0, this.cursor.Width, this.cursor.Height),
                Color.White,
                MathHelper.ToRadians(0),
                new Vector2(this.cursor.Width / 2, this.cursor.Height / 2),
                1f,
                SpriteEffects.None,
                0);
            spriteBatch.End();
        }

        /// <summary>
        /// Draw points
        /// </summary>
        /// <param name="spriteBatch">The SpriteBatch object</param>
        public void DrawPoints(SpriteBatch spriteBatch)
        {
            SpriteFont font = LoadedContent.GetFont("Calibri");
            spriteBatch.Begin();
            spriteBatch.DrawString(font, GameRules.GetTotalPoints().ToString(), new Vector2(10, 10), Color.Green);
            spriteBatch.End();
        }

        /// <summary>
        /// Update the menu
        /// </summary>
        /// <param name="gameTime">The GameTime object</param>
        public void Update(GameTime gameTime)
        {
            this.buttons.ForEach(button => button.Update(gameTime));
        }
    }
}