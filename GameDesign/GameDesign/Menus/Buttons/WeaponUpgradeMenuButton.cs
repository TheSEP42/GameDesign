﻿using GameDesign.Menus.ButtonBases;
using GameDesign.Models.Weapons;
using GameDesign.Rules;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameDesign.Menus.Buttons
{
    /// <summary>
    /// Weapon upgrade button
    /// </summary>
    internal class WeaponUpgradeMenuButton : UpgradeMenuButton
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WeaponUpgradeMenuButton"/> class
        /// </summary>
        /// <param name="position">    The position of the button</param>
        /// <param name="content">     The content of the button</param>
        /// <param name="scale">       The scale of the button</param>
        /// <param name="type">        The type of upgrade</param>
        /// <param name="upgradeLevel">The upgrade level</param>
        /// <param name="cost">        The cost to use</param>
        public WeaponUpgradeMenuButton(
            Vector2 position,
            Texture2D content,
            float scale,
            WeaponType type,
            int upgradeLevel,
            int cost)
            : base(position, content, scale, type, upgradeLevel, cost)
        {
        }

        /// <summary>
        /// Update the button
        /// </summary>
        /// <param name="gameTime">The GameTime object</param>
        public override void Update(GameTime gameTime)
        {
            this.activeUpgradeLevel = GameRules.WeaponUpgradeLevel((WeaponType)this.upgradeType);
            base.Update(gameTime);
        }

        /// <summary>
        /// Press this button
        /// </summary>
        protected override void PressAction()
        {
            GameRules.UpgradeWeapon((WeaponType)this.upgradeType);
        }
    }
}