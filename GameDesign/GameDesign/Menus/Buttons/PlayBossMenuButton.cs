﻿using GameDesign.Content;
using GameDesign.Menus.ButtonBases;
using GameDesign.Rules;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameDesign.Menus.Buttons
{
    /// <summary>
    /// Play Boss menu button
    /// </summary>
    internal class PlayBossMenuButton : MenuButton
    {
        /// <summary>
        /// Normal color of the button
        /// </summary>
        protected Color normalColor = Color.Green;

        /// <summary>
        /// State of the button
        /// </summary>
        protected MenuButtonState state = MenuButtonState.Unavailable;

        /// <summary>
        /// Color when button is unavailable
        /// </summary>
        protected Color unavailableColor = Color.Red;

        /// <summary>
        /// Outline when available
        /// </summary>
        private Texture2D availableOutline;

        /// <summary>
        /// Outline when unavailable
        /// </summary>
        private Texture2D unavailableOutline;

        /// <summary>
        /// Initializes a new instance of the PlayBossMenuButton class
        /// </summary>
        /// <param name="position">Position of the button</param>
        /// <param name="color">   Color of the button</param>
        /// <param name="scale">   Scale of the button</param>
        public PlayBossMenuButton(
            Vector2 position,
            Color color,
            float scale) : base(position, LoadedContent.GetTexture("LongButtonOutline"), LoadedContent.GetTexture("PlayBoss"), color, scale)
        {
            this.availableOutline = LoadedContent.GetTexture("LongButtonOutline");
            this.unavailableOutline = LoadedContent.GetTexture("LongButtonOutlineCrossed");
        }

        /// <summary>
        /// Draw the button
        /// </summary>
        /// <param name="spriteBatch">The SpriteBatch object</param>
        public override void Draw(SpriteBatch spriteBatch)
        {
            this.outline = (this.state == MenuButtonState.Unavailable) ? this.unavailableOutline : this.availableOutline;
            base.Draw(spriteBatch);
        }

        /// <summary>
        /// Update the button
        /// </summary>
        /// <param name="gameTime">The GameTime object</param>
        public override void Update(GameTime gameTime)
        {
            int currentLevel = GameRules.LevelCount;
            if (currentLevel < 2)
            {
                this.state = MenuButtonState.Unavailable;
            }
            else
            {
                this.state = MenuButtonState.Available;
            }

            this.UpdateColor();
            base.Update(gameTime);
        }

        /// <summary>
        /// Check if the mouse is over the button
        /// </summary>
        /// <param name="gameTime">The GameTime object</param>
        protected override void CheckHover(GameTime gameTime)
        {
            if (this.state == MenuButtonState.Available && this.MouseOver())
            {
                this.Lift(gameTime);
            }
            else
            {
                this.Release(gameTime);
            }
        }

        /// <summary>
        /// Check if the button is pressed
        /// </summary>
        /// <param name="gameTime">The GameTime object</param>
        protected override void CheckPress(GameTime gameTime)
        {
            if (this.state == MenuButtonState.Available)
            {
                base.CheckPress(gameTime);
            }
        }

        /// <summary>
        /// Press this button
        /// </summary>
        protected override void Press()
        {
            if (this.state == MenuButtonState.Available)
            {
                GameRules.StartBossLevel();
            }
        }

        /// <summary>
        /// Update the color of this button
        /// </summary>
        private void UpdateColor()
        {
            switch (this.state)
            {
                case MenuButtonState.Unavailable:
                    this.color = this.unavailableColor;
                    break;

                case MenuButtonState.Available:
                    this.color = this.normalColor;
                    break;

                default:
                    this.color = this.normalColor;
                    break;
            }
        }
    }
}