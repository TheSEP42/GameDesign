﻿using GameDesign.Menus.ButtonBases;
using GameDesign.Models.Powerups;
using GameDesign.Rules;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameDesign.Menus.Buttons
{
    /// <summary>
    /// Power up Upgrade button
    /// </summary>
    internal class PowerupUpgradeMenuButton : UpgradeMenuButton
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PowerupUpgradeMenuButton"/> class
        /// </summary>
        /// <param name="position">    The position of the button</param>
        /// <param name="content">     The content of the button</param>
        /// <param name="scale">       The scale of the button</param>
        /// <param name="type">        The type of upgrade</param>
        /// <param name="upgradeLevel">The upgrade level</param>
        /// <param name="cost">        The cost to use</param>
        public PowerupUpgradeMenuButton(
            Vector2 position,
            Texture2D content,
            float scale,
            PowerupType type,
            int upgradeLevel,
            int cost)
            : base(position, content, scale, type, upgradeLevel, cost)
        {
        }

        /// <summary>
        /// Update this button
        /// </summary>
        /// <param name="gameTime">The GameTime object</param>
        public override void Update(GameTime gameTime)
        {
            this.activeUpgradeLevel = GameRules.PowerupUpgradeLevel((PowerupType)this.upgradeType);
            base.Update(gameTime);
        }

        /// <summary>
        /// Press this button
        /// </summary>
        protected override void PressAction()
        {
            GameRules.UpgradePowerup((PowerupType)this.upgradeType);
        }
    }
}