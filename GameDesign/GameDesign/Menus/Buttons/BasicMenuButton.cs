﻿using System;
using GameDesign.Content;
using GameDesign.Menus.ButtonBases;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameDesign.Menus.Buttons
{
    /// <summary>
    /// Basic menu button
    /// </summary>
    internal class BasicMenuButton : MenuButton
    {
        /// <summary>
        /// Action when the button is pressed
        /// </summary>
        private Action pressAction;

        /// <summary>
        /// Initializes a new instance of the BasicMenuButton class
        /// </summary>
        /// <param name="position">Position of the button</param>
        /// <param name="color">   Color of the button</param>
        /// <param name="scale">   Scale of the button</param>
        /// <param name="action">  Action when button is pressed</param>
        /// <param name="content"> Content of the button</param>
        public BasicMenuButton(
            Vector2 position,
            Color color,
            float scale,
            Action action,
            Texture2D content) : base(position, LoadedContent.GetTexture("LongButtonOutline"), content, color, scale)
        {
            this.pressAction = action;
        }

        /// <summary>
        /// Initializes a new instance of the BasicMenuButton class
        /// </summary>
        /// <param name="position">Position of the button</param>
        /// <param name="color">   Color of the button</param>
        /// <param name="scale">   Scale of the button</param>
        /// <param name="action">  Action when button is pressed</param>
        /// <param name="outline"> Outline of the button</param>
        /// <param name="content"> Content of the button</param>
        public BasicMenuButton(
            Vector2 position,
            Color color,
            float scale,
            Action action,
            Texture2D outline,
            Texture2D content) : base(position, outline, content, color, scale)
        {
            this.pressAction = action;
        }

        /// <summary>
        /// Press this button
        /// </summary>
        protected override void Press()
        {
            this.pressAction();
        }
    }
}