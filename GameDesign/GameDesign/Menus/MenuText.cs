﻿using GameDesign.Helpers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameDesign.Menus
{
    /// <summary>
    /// Text for menus
    /// </summary>
    public class MenuText
    {
        /// <summary>
        /// Alignment of text
        /// </summary>
        private Alignment alignment;

        /// <summary>
        /// Bounds of text
        /// </summary>
        private Rectangle bounds;

        /// <summary>
        /// Color of text
        /// </summary>
        private Color color;

        /// <summary>
        /// Text font
        /// </summary>
        private SpriteFont font;

        /// <summary>
        /// Text string
        /// </summary>
        private string text;

        /// <summary>
        /// Initializes a new instance of the MenuText class
        /// </summary>
        /// <param name="text">     Text to draw</param>
        /// <param name="color">    Color of the text</param>
        /// <param name="bounds">   Bounds of the text</param>
        /// <param name="alignment">Alignment of the text</param>
        /// <param name="font">     Font of the text</param>
        public MenuText(string text, Color color, Rectangle bounds, Alignment alignment, SpriteFont font)
        {
            this.text = text;
            this.color = color;
            this.bounds = bounds;
            this.alignment = alignment;
            this.font = font;
        }

        /// <summary>
        /// Draw the text
        /// </summary>
        /// <param name="spriteBatch">the SpriteBatch object</param>
        public void Draw(SpriteBatch spriteBatch)
        {
            StringHelper.DrawString(spriteBatch, this.font, this.text, this.bounds, this.alignment, this.color);
        }
    }
}