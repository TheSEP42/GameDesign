﻿using System;
using System.Collections.Generic;
using GameDesign.Helpers;
using GameDesign.Managers;
using GameDesign.Rules;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameDesign.Models
{
    /// <summary>
    /// Pertains to all spritely information
    /// </summary>
    public abstract class Sprite
    {
        /// <summary>
        /// Angle of the sprite
        /// </summary>
        public float Angle;

        /// <summary>
        /// Damage caused by the sprite
        /// </summary>
        public int Damage = 0;

        /// <summary>
        /// Display information
        /// </summary>
        public DisplayInformation DisplayInfo;

        /// <summary>
        /// Is this sprite invulnerable
        /// </summary>
        public bool IsInvulnerable = false;

        /// <summary>
        /// Position of the sprite
        /// </summary>
        public Vector2 Position;

        /// <summary>
        /// Collision radius
        /// </summary>
        public float Radius;

        /// <summary>
        /// Acceleration of the sprite
        /// </summary>
        protected float acceleration = 5.0f;

        /// <summary>
        /// Can this sprite wrap the screen
        /// </summary>
        protected bool canWrap = false;

        /// <summary>
        /// Length of the sprite's death animation
        /// </summary>
        protected float deathAnimationLength = 0f;

        /// <summary>
        /// Timer for tracking death animation
        /// </summary>
        protected float deathAnimationTimer = 0f;

        /// <summary>
        /// Life span of the sprite
        /// </summary>
        protected float lifeSpan;

        /// <summary>
        /// Maximum velocity of the sprite
        /// </summary>
        protected float maxVelocity = 3f;

        /// <summary>
        /// Rotation speed of the sprite
        /// </summary>
        protected float rotationSpeed = MathHelper.ToRadians(180f);

        /// <summary>
        /// Current health of the sprite
        /// </summary>
        private int health = 1;

        /// <summary>
        /// Maximum health of this sprite
        /// </summary>
        private int maxHealth = 1;

        /// <summary>
        /// Time the sprite was created
        /// </summary>
        private float? timeCreated;

        /// <summary>
        /// Initializes a new instance of the Sprite class
        /// </summary>
        /// <param name="position"> Position of the sprite</param>
        /// <param name="direction">Direction of the sprite</param>
        /// <param name="scale">    Scale of the sprite</param>
        public Sprite(Vector2 position, float direction, float scale)
        {
            this.IsDeleted = false;
            this.Velocity = new Vector2(0, 0);
            this.Position = position;
            this.Angle = direction;
            this.Damage = 0;

            this.DisplayInfo.Color = Color.White;
            this.DisplayInfo.UnHealthyColor = Color.White;
            this.DisplayInfo.Scale = scale;
        }

        /// <summary>
        /// Initializes a new instance of the Sprite class
        /// </summary>
        /// <param name="position"> The Position of the sprite to be drawn</param>
        /// <param name="direction">The direction for the sprite to face</param>
        public Sprite(Vector2 position, float direction) : this(position, direction, 1f)
        {
        }

        /// <summary>
        /// Gets the current death animation percentage.
        /// </summary>
        public float DeathAnimationPercent
        {
            get { return (float)this.deathAnimationTimer / (float)this.deathAnimationLength; }
        }

        /// <summary>
        /// Gets the vector representation of the facing angle
        /// </summary>
        public Vector2 DirectionVector
        {
            get { return new Vector2((float)Math.Sin(this.Angle), -(float)Math.Cos(this.Angle)); }
        }

        /// <summary>
        /// Sets the health of this sprite
        /// </summary>
        public int Health
        {
            set
            {
                this.maxHealth = value;
                this.health = value;
            }
        }

        /// <summary>
        /// Gets the current health percentage.
        /// </summary>
        public float HealthPercent
        {
            get { return (float)this.health / (float)this.maxHealth; }
        }

        /// <summary>
        /// Gets a value indicating whether this sprite has been destroyed.
        /// </summary>
        public bool IsDead
        {
            get { return this.health <= 0; }
        }

        /// <summary>
        /// Gets a value indicating whether this sprite ready to be deleted.
        /// </summary>
        public bool IsDeleted
        {
            get; private set;
        }

        /// <summary>
        /// Gets the center of the sprite
        /// </summary>
        public Vector2 Origin
        {
            get { return new Vector2(this.DisplayInfo.Texture.Width, this.DisplayInfo.Texture.Height) / 2; }
        }

        /// <summary>
        /// Gets or sets the velocity vector of the sprite
        /// </summary>
        public Vector2 Velocity
        {
            get; protected set;
        }

        /// <summary>
        /// Accelerate the sprite in the current facing direction
        /// </summary>
        /// <param name="gameTime">The GameTime object</param>
        public void Accelerate(GameTime gameTime)
        {
            float acceleration = this.acceleration * GameTimeHelper.GetTimeSinceLastTick(gameTime);
            this.Velocity += acceleration * this.DirectionVector;

            if (this.Velocity.Length() > this.maxVelocity)
            {
                Vector2 velocityDirection = this.Velocity;
                velocityDirection.Normalize();
                this.Velocity = velocityDirection * this.maxVelocity;
            }
        }

        /// <summary>
        /// Draw the sprite to the screen
        /// </summary>
        /// <param name="spriteBatch">The SpriteBatch object</param>
        public virtual void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(
                this.DisplayInfo.Texture,
                this.Position,
                this.DisplayInfo.DrawRectangle,
                this.DisplayInfo.CurrentColor,
                this.Angle,
                this.Origin,
                this.DisplayInfo.Scale,
                SpriteEffects.None,
                0f);

            List<Texture2D> overlays = this.DisplayInfo.GetOverlayTextures();
            if (overlays.Count > 0)
            {
                foreach (Texture2D overlay in overlays)
                {
                    spriteBatch.Draw(
                        overlay,
                        this.Position,
                        new Rectangle(0, 0, overlay.Width, overlay.Height),
                        Color.White,
                        this.Angle,
                        new Vector2(overlay.Width / 2, overlay.Height / 2),
                        this.DisplayInfo.Scale,
                        SpriteEffects.None,
                        0f);
                }
            }
        }

        /// <summary>
        /// Heal the sprite
        /// </summary>
        /// <param name="value">The value to heal this sprite by</param>
        public virtual void Heal(int value)
        {
            this.health += value;
            if (this.health > this.maxHealth)
            {
                this.health = this.maxHealth;
            }
        }

        /// <summary>
        /// Instantly delete this sprite
        /// </summary>
        public virtual void InstantDelete()
        {
            this.IsDeleted = true;
        }

        /// <summary>
        /// Rotate the sprite in the anti-clockwise direction
        /// </summary>
        /// <param name="gameTime">     The GameTime object</param>
        /// <param name="rotationSpeed">The speed to rotate the sprite (radians)</param>
        public void RotateAntiClockwise(GameTime gameTime, float rotationSpeed)
        {
            this.Angle -= rotationSpeed * (gameTime.ElapsedGameTime.Milliseconds * Globals.MILLICONST);
        }

        /// <summary>
        /// Rotate the sprite in the clockwise direction
        /// </summary>
        /// <param name="gameTime">     The GameTime object</param>
        /// <param name="rotationSpeed">The speed to rotate the sprite (radians)</param>
        public void RotateClockwise(GameTime gameTime, float rotationSpeed)
        {
            this.Angle += rotationSpeed * (gameTime.ElapsedGameTime.Milliseconds * Globals.MILLICONST);
        }

        /// <summary>
        /// Damage the sprite
        /// </summary>
        /// <param name="sprite">Sprite to take damage from</param>
        public virtual void TakeDamage(Sprite sprite)
        {
            if (!this.IsInvulnerable)
            {
                this.health -= sprite.Damage;
            }
        }

        /// <summary>
        /// Update the sprite
        /// </summary>
        /// <param name="gameTime">The GameTime Object</param>
        public virtual void Update(GameTime gameTime)
        {
            this.Position.X += this.Velocity.X;
            this.Position.Y += this.Velocity.Y;
            if (this.canWrap)
            {
                this.Position.X = (this.Position.X + Globals.WIDTH) % Globals.WIDTH;
                this.Position.Y = (this.Position.Y + Globals.HEIGHT) % Globals.HEIGHT;
            }

            this.UpdateColor();
            this.CheckLifeSpan(gameTime);
            this.CheckDeath(gameTime);
            if (this.IsDead)
            {
                this.DeathAnimation(gameTime);
            }
        }

        /// <summary>
        /// Create particle effects
        /// </summary>
        /// <param name="gameTime">             The GameTime object</param>
        /// <param name="volumePerSecond">      The volume of particles generated per second</param>
        /// <param name="spray">                The angle of spray in degrees</param>
        /// <param name="lifeSpan">             Life span of the particles</param>
        /// <param name="color">                Color of the particles</param>
        /// <param name="velocity">             Velocity of the particles</param>
        /// <param name="velocityAccuracyLevel">Velocity accuracy level</param>
        protected void CreateParticleEffects(GameTime gameTime, int volumePerSecond, int spray, float lifeSpan, Color color, float velocity, int velocityAccuracyLevel)
        {
            int create = (int)((float)gameTime.ElapsedGameTime.Milliseconds * Globals.MILLICONST * volumePerSecond);
            Random random = new Random();
            for (int i = 0; i < create; i++)
            {
                float angleAccuracy = ((float)random.NextDouble() * spray) - (spray / 2);
                float directionOffset = MathHelper.ToRadians(angleAccuracy);
                float velocityOffset = ((float)random.NextDouble() * velocityAccuracyLevel) - (velocityAccuracyLevel / 2);

                Particle particle = new Particle(this.Position, this.Angle - MathHelper.ToRadians(180f) + directionOffset, this.Velocity, velocity + velocityOffset, color, lifeSpan);
                EntityManager.Add(particle);
            }
        }

        /// <summary>
        /// Render changes for death animation
        /// </summary>
        /// <param name="gameTime">The GameTime object</param>
        protected virtual void DeathAnimation(GameTime gameTime)
        {
            this.deathAnimationTimer += GameTimeHelper.GetTimeSinceLastTick(gameTime);
            if (this.deathAnimationTimer > this.deathAnimationLength)
            {
                this.IsDeleted = true;
            }
        }

        /// <summary>
        /// Get the life percentage of the sprite
        /// </summary>
        /// <param name="gameTime">The GameTime object</param>
        /// <returns>Returns a float between 0 and 1 indicating the percentage</returns>
        protected float GetLifePercentage(GameTime gameTime)
        {
            float currentGameTime = GameTimeHelper.GetCurrentGameTime(gameTime);
            if (this.lifeSpan != 0f && this.timeCreated != null)
            {
                float percent = (currentGameTime - (float)this.timeCreated) / this.lifeSpan;
                if (percent < 0)
                {
                    percent = 0;
                }

                if (percent > 1)
                {
                    percent = 1;
                }

                return percent;
            }
            else
            {
                return 1;
            }
        }

        /// <summary>
        /// Kill the sprite
        /// </summary>
        protected virtual void Kill()
        {
            this.health = 0;
        }

        /// <summary>
        /// Update the color of the sprite based on health percentage
        /// </summary>
        protected void UpdateColor()
        {
            if (this.DisplayInfo.UnHealthyColor == null)
            {
                this.DisplayInfo.UnHealthyColor = this.DisplayInfo.HealthyColor;
            }

            Vector3 colorVector = this.DisplayInfo.HealthyColor.ToVector3() - this.DisplayInfo.UnHealthyColor.ToVector3();
            colorVector *= 1 - this.HealthPercent;
            Vector3 newColor = this.DisplayInfo.HealthyColor.ToVector3() - colorVector;
            this.DisplayInfo.CurrentColor = new Color(newColor);
        }

        /// <summary>
        /// Check health of sprite and update death variables accordingly
        /// </summary>
        /// <param name="gameTime">the GameTime object</param>
        private void CheckDeath(GameTime gameTime)
        {
            if (this.IsDead)
            {
                this.Kill();
            }
        }

        /// <summary>
        /// Check LifeSpan to see if sprite is still alive
        /// </summary>
        /// <param name="gameTime">The GameTime object</param>
        private void CheckLifeSpan(GameTime gameTime)
        {
            float currentGameTime = GameTimeHelper.GetCurrentGameTime(gameTime);
            if (this.lifeSpan != 0f)
            {
                if (this.timeCreated == null)
                {
                    this.timeCreated = currentGameTime;
                }
                else if (currentGameTime > this.timeCreated + this.lifeSpan)
                {
                    this.Kill();
                }
            }
        }

        /// <summary>
        /// All texture display information
        /// </summary>
        public struct DisplayInformation
        {
            /// <summary>
            /// Colors of the sprite
            /// </summary>
            public Color
                HealthyColor,
                UnHealthyColor,
                CurrentColor;

            /// <summary>
            /// Sprite texture
            /// </summary>
            public Texture2D Texture;

            /// <summary>
            /// Current scale of the sprite
            /// </summary>
            private float currentScale;

            /// <summary>
            /// The draw rectangle
            /// </summary>
            private Rectangle? drawRectangle;

            /// <summary>
            /// The initial scale of the sprite
            /// </summary>
            private float? initialScale;

            /// <summary>
            /// List of overlay textures
            /// </summary>
            private List<Texture2D> overlayTextures;

            /// <summary>
            /// Sets the healthy and current color.
            /// </summary>
            public Color Color
            {
                set
                {
                    this.HealthyColor = value;
                    this.CurrentColor = value;
                }
            }

            /// <summary>
            /// Gets or sets the draw rectangle;
            /// </summary>
            public Rectangle DrawRectangle
            {
                get
                {
                    if (this.drawRectangle.HasValue)
                    {
                        return this.drawRectangle.Value;
                    }
                    else
                    {
                        return new Rectangle(0, 0, this.Texture.Width, this.Texture.Height);
                    }
                }

                set
                {
                    this.drawRectangle = value;
                }
            }

            /// <summary>
            /// Gets the first scale the sprite was set to
            /// </summary>
            public float InitialScale
            {
                get
                {
                    if (this.initialScale.HasValue)
                    {
                        return this.initialScale.Value;
                    }
                    else
                    {
                        return 0f;
                    }
                }
            }

            /// <summary>
            /// Gets or sets the scale
            /// </summary>
            public float Scale
            {
                get
                {
                    return this.currentScale;
                }

                set
                {
                    this.currentScale = value;
                    if (!this.initialScale.HasValue)
                    {
                        this.initialScale = value;
                    }
                }
            }

            /// <summary>
            /// Add overlay to sprite
            /// </summary>
            /// <param name="overlay">The texture to overlay</param>
            public void AddOverlay(Texture2D overlay)
            {
                if (this.overlayTextures == null)
                {
                    this.overlayTextures = new List<Texture2D>();
                }

                this.overlayTextures.Add(overlay);
            }

            /// <summary>
            /// Returns a value indicating whether the current overlay is contained
            /// </summary>
            /// <param name="overlay">Texture to check</param>
            /// <returns>Value indicating whether overlay is contained</returns>
            public bool ContainsOverlay(Texture2D overlay)
            {
                if (this.overlayTextures != null)
                {
                    return this.overlayTextures.Contains(overlay);
                }
                else
                {
                    return false;
                }
            }

            /// <summary>
            /// Return list of all overlay textures
            /// </summary>
            /// <returns>List of Textures</returns>
            public List<Texture2D> GetOverlayTextures()
            {
                if (this.overlayTextures != null)
                {
                    return this.overlayTextures;
                }
                else
                {
                    return new List<Texture2D>();
                }
            }

            /// <summary>
            /// Remove overlay from sprite
            /// </summary>
            /// <param name="overlay">The texture to remove</param>
            public void RemoveOverlay(Texture2D overlay)
            {
                if (this.overlayTextures != null)
                {
                    this.overlayTextures.Remove(overlay);
                }
            }
        }
    }
}