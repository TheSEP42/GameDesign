﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameDesign.Models
{
    /// <summary>
    /// Sprite that cannot move
    /// </summary>
    public class StaticSprite : Sprite
    {
        /// <summary>
        /// Initializes a new instance of the StaticSprite class
        /// </summary>
        /// <param name="position"> Position of the sprite</param>
        /// <param name="direction">Direction of the sprite</param>
        /// <param name="texture">  Texture of the sprite</param>
        public StaticSprite(Vector2 position, float direction, Texture2D texture) : base(position, direction)
        {
            this.DisplayInfo.Texture = texture;
        }

        /// <summary>
        /// When called no update can take place because this sprite cannot move
        /// </summary>
        /// <param name="gameTime">The GameTime object</param>
        public override void Update(GameTime gameTime)
        {
            // Do nothing ever
        }
    }
}