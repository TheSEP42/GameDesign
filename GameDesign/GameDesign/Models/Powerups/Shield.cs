﻿using GameDesign.Content;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameDesign.Models.Powerups
{
    /// <summary>
    /// Shield power up
    /// </summary>
    internal class Shield : Powerup
    {
        /// <summary>
        /// Overlay texture
        /// </summary>
        private Texture2D overlay;

        /// <summary>
        /// Initializes a new instance of the <see cref="Shield"/> class
        /// </summary>
        /// <param name="position">    Position of the shield</param>
        /// <param name="upgradeLevel">Upgrade level of the shield</param>
        public Shield(Vector2 position, int upgradeLevel) : base(position, upgradeLevel)
        {
            this.DisplayInfo.Texture = LoadedContent.GetTexture("Shield");
            this.type = PowerupType.Shield;
            this.overlay = LoadedContent.GetTexture("ShieldOverlay");
            this.lifeSpan = 30f;
            this.Radius = 10f;
            this.baseApplicationLength = 5f;
            this.applicationLength = this.baseApplicationLength;
        }

        /// <summary>
        /// Apply the power up
        /// </summary>
        /// <param name="sprite">Sprite to apply power up to</param>
        public override void Apply(Sprite sprite)
        {
            base.Apply(sprite);
            if (this.timer < this.baseApplicationLength)
            {
                sprite.IsInvulnerable = true;
                if (!sprite.DisplayInfo.ContainsOverlay(this.overlay))
                {
                    sprite.DisplayInfo.AddOverlay(this.overlay);
                }
            }
            else
            {
                sprite.IsInvulnerable = false;
                sprite.DisplayInfo.RemoveOverlay(this.overlay);
                this.Kill();
            }
        }

        /// <summary>
        /// Power up level 0
        /// </summary>
        protected override void PowerupLevel0()
        {
        }

        /// <summary>
        /// Power up level 1
        /// </summary>
        protected override void PowerupLevel1()
        {
            this.applicationLength = this.baseApplicationLength += 5f;
        }

        /// <summary>
        /// Power up level 2
        /// </summary>
        protected override void PowerupLevel2()
        {
            this.applicationLength = this.baseApplicationLength += 10f;
        }

        /// <summary>
        /// Power up level 3
        /// </summary>
        protected override void PowerupLevel3()
        {
            this.applicationLength = this.baseApplicationLength += 20f;
        }
    }
}