﻿namespace GameDesign.Models.Powerups
{
    /// <summary>
    /// Type of power up
    /// </summary>
    public enum PowerupType
    {
        /// <summary>
        /// Health pack
        /// </summary>
        Health = 0,

        /// <summary>
        /// Shield add-on
        /// </summary>
        Shield = 1
    }
}