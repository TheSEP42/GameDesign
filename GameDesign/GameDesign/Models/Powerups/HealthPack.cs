﻿using System;
using GameDesign.Content;
using GameDesign.Rules;
using Microsoft.Xna.Framework;

namespace GameDesign.Models.Powerups
{
    /// <summary>
    /// Health pack sprite
    /// </summary>
    internal class HealthPack : Powerup
    {
        /// <summary>
        /// Base boost to health
        /// </summary>
        private float baseHealthBoost;

        /// <summary>
        /// Active boost to health
        /// </summary>
        private int healthBoost;

        /// <summary>
        /// Initializes a new instance of the <see cref="HealthPack"/> class
        /// </summary>
        /// <param name="position">    Position of the health pack</param>
        /// <param name="upgradeLevel">Upgrade level of the health pack</param>
        public HealthPack(Vector2 position, int upgradeLevel) : base(position, upgradeLevel)
        {
            this.DisplayInfo.Texture = LoadedContent.GetTexture("HealthPack");
            this.type = PowerupType.Health;
            this.baseHealthBoost = 0.1f;
            this.baseLifeSpan = 10f;
            this.healthBoost = Convert.ToInt32(GameRules.PlayerHealth * 0.1f);
            this.lifeSpan = 10f;
            this.Radius = 10f;
        }

        /// <summary>
        /// Apply the health boost
        /// </summary>
        /// <param name="sprite">Sprite to apply the health boost to</param>
        public override void Apply(Sprite sprite)
        {
            base.Apply(sprite);
            sprite.Heal(this.healthBoost);
            this.Kill();
        }

        /// <summary>
        /// Upgrade level 0
        /// </summary>
        protected override void PowerupLevel0()
        {
            this.healthBoost = Convert.ToInt32(GameRules.PlayerHealth * this.baseHealthBoost);
        }

        /// <summary>
        /// Upgrade level 1
        /// </summary>
        protected override void PowerupLevel1()
        {
            this.healthBoost = Convert.ToInt32(GameRules.PlayerHealth * 0.2f);
        }

        /// <summary>
        /// Upgrade level 2
        /// </summary>
        protected override void PowerupLevel2()
        {
            this.healthBoost = Convert.ToInt32(GameRules.PlayerHealth * 0.5f);
        }

        /// <summary>
        /// Upgrade level 3
        /// </summary>
        protected override void PowerupLevel3()
        {
            this.healthBoost = Convert.ToInt32(GameRules.PlayerHealth);
        }
    }
}