﻿using System;
using GameDesign.Helpers;
using GameDesign.Rules;
using Microsoft.Xna.Framework;

namespace GameDesign.Models.Powerups
{
    /// <summary>
    /// Power up class
    /// </summary>
    internal abstract class Powerup : Sprite
    {
        /// <summary>
        /// Is the power up active
        /// </summary>
        protected bool activated = false;

        /// <summary>
        /// Length to apply the power up
        /// </summary>
        protected float applicationLength;

        /// <summary>
        /// Base application length
        /// </summary>
        protected float baseApplicationLength;

        /// <summary>
        /// Base life span
        /// </summary>
        protected float baseLifeSpan;

        /// <summary>
        /// Timer to keep track of application length
        /// </summary>
        protected float timer = 0;

        /// <summary>
        /// Type of power up
        /// </summary>
        protected PowerupType type;

        /// <summary>
        /// Upgrade level
        /// </summary>
        protected int upgradeLevel;

        /// <summary>
        /// Initializes a new instance of the <see cref="Powerup"/> class
        /// </summary>
        /// <param name="position">    Position of the power up</param>
        /// <param name="upgradeLevel">Upgrade level of the power up</param>
        public Powerup(Vector2 position, int upgradeLevel) : base(position, 0)
        {
            this.upgradeLevel = upgradeLevel;
            this.deathAnimationLength = 0.2f;
        }

        /// <summary>
        /// Apply the power up
        /// </summary>
        /// <param name="sprite">Sprite to apply the power up to</param>
        public virtual void Apply(Sprite sprite)
        {
            int upgradeLevel = GameRules.PowerupUpgradeLevel(this.type);
            switch (upgradeLevel)
            {
                case 0:
                    this.PowerupLevel0();
                    break;

                case 1:
                    this.PowerupLevel1();
                    break;

                case 2:
                    this.PowerupLevel2();
                    break;

                case 3:
                    this.PowerupLevel3();
                    break;

                default:
                    this.PowerupLevel0();
                    break;
            }

            this.activated = true;
        }

        /// <summary>
        /// Update the power up
        /// </summary>
        /// <param name="gameTime">The GameTime object</param>
        public override void Update(GameTime gameTime)
        {
            if (this.activated)
            {
                this.timer += GameTimeHelper.GetTimeSinceLastTick(gameTime);
            }

            base.Update(gameTime);
        }

        /// <summary>
        /// Animate the death of the power up
        /// </summary>
        /// <param name="gameTime">The GameTime object</param>
        protected override void DeathAnimation(GameTime gameTime)
        {
            base.DeathAnimation(gameTime);
            float currentTime = GameTimeHelper.GetCurrentGameTime(gameTime);
            this.DisplayInfo.Scale = (1 - this.DeathAnimationPercent) * this.DisplayInfo.InitialScale;

            if (this.deathAnimationTimer > this.deathAnimationLength)
            {
                this.CreateParticleEffects(gameTime, 1000, 359, 1f, Color.Red, 20, 20);
            }
        }

        /// <summary>
        /// Power up level 0
        /// </summary>
        protected abstract void PowerupLevel0();

        /// <summary>
        /// Power up level 1
        /// </summary>
        protected abstract void PowerupLevel1();

        /// <summary>
        /// Power up level 2
        /// </summary>
        protected abstract void PowerupLevel2();

        /// <summary>
        /// Power up level 3
        /// </summary>
        protected abstract void PowerupLevel3();
    }
}