﻿using GameDesign.Content;
using Microsoft.Xna.Framework;

namespace GameDesign.Models.Projectiles
{
    /// <summary>
    /// Bullet sprite
    /// </summary>
    internal class Bullet : Projectile
    {
        /// <summary>
        /// Initializes a new instance of the Bullet class
        /// </summary>
        /// <param name="position"> Position of the bullet</param>
        /// <param name="direction">Direction of the bullet</param>
        /// <param name="velocity"> Inherited velocity of the bullet</param>
        public Bullet(Vector2 position, float direction, Vector2 velocity) : base(position, direction, velocity)
        {
            this.ProjectileVelocity = 800;
            this.DisplayInfo.Texture = LoadedContent.GetTexture("Bullet");
            this.Radius = 2;
            this.Damage = 10;
            this.canWrap = true;
            this.lifeSpan = 1.3f;
        }
    }
}