﻿namespace GameDesign.Models.Projectiles
{
    /// <summary>
    /// Types of projectile
    /// </summary>
    internal enum ProjectileType
    {
        /// <summary>
        /// Bullet projectile
        /// </summary>
        Bullet = 0,

        /// <summary>
        /// Rocket projectile
        /// </summary>
        Rocket = 1
    }
}