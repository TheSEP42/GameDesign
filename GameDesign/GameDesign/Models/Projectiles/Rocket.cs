﻿using System;
using GameDesign.Content;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;

namespace GameDesign.Models.Projectiles
{
    /// <summary>
    /// Rocket projectile
    /// </summary>
    internal class Rocket : Projectile
    {
        /// <summary>
        /// Initializes a new instance of the Rocket class
        /// </summary>
        /// <param name="position"> Position of the rocket</param>
        /// <param name="direction">Direction of the rocket</param>
        /// <param name="velocity"> Inherited velocity from creator</param>
        public Rocket(Vector2 position, float direction, Vector2 velocity) : base(position, direction, velocity)
        {
            this.ProjectileVelocity = 0;
            this.DisplayInfo.Texture = LoadedContent.GetTexture("Rocket");
            this.Radius = 5;
            this.Damage = 100;
            this.maxVelocity = 10f;
            this.acceleration = 10f;
            this.Velocity = this.inheritedvelocity;
            this.accelerates = true;
            this.canWrap = true;
            this.lifeSpan = 3f;
        }

        /// <summary>
        /// Update the rocket
        /// </summary>
        /// <param name="gameTime">The GameTime object</param>
        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            this.CreateParticleEffects(gameTime, 100, 60, 1, Color.Red, 100, 20);
        }

        /// <summary>
        /// Animate the death of the projectile
        /// </summary>
        /// <param name="gameTime">The GameTime object</param>
        protected override void DeathAnimation(GameTime gameTime)
        {
            base.DeathAnimation(gameTime);
            if (this.deathAnimationTimer > this.deathAnimationLength)
            {
                Random rand = new Random();
                double chanceResult = rand.NextDouble();
                SoundEffectInstance explosion;
                if (chanceResult < 1 / 3)
                {
                    explosion = LoadedContent.GetSoundEffect("Explosion1");
                }
                else if (chanceResult < (1 / 3) * 2)
                {
                    explosion = LoadedContent.GetSoundEffect("Explosion2");
                }
                else
                {
                    explosion = LoadedContent.GetSoundEffect("Explosion3");
                }

                explosion.Volume = 0.1f;
                explosion.Play();
            }
        }
    }
}