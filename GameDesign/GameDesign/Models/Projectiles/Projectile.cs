﻿using GameDesign.Helpers;
using GameDesign.Rules;
using Microsoft.Xna.Framework;

namespace GameDesign.Models.Projectiles
{
    /// <summary>
    /// Projectile sprite
    /// </summary>
    internal class Projectile : Sprite
    {
        /// <summary>
        /// Does this projectile accelerate
        /// </summary>
        protected bool accelerates = false;

        /// <summary>
        /// Inherited velocity from creator
        /// </summary>
        protected Vector2 inheritedvelocity;

        /// <summary>
        /// Velocity of the projectile
        /// </summary>
        private float projectileVelocity;

        /// <summary>
        /// Initializes a new instance of the Projectile class
        /// </summary>
        /// <param name="position">         Position of the projectile</param>
        /// <param name="direction">        Direction of the projectile</param>
        /// <param name="inheritedVelocity">Inherited velocity from creator</param>
        public Projectile(Vector2 position, float direction, Vector2 inheritedVelocity) : base(position, direction)
        {
            this.inheritedvelocity = inheritedVelocity;
        }

        /// <summary>
        /// Sets the Velocity of this projectile
        /// </summary>
        protected float ProjectileVelocity
        {
            set { this.projectileVelocity = value; }
        }

        /// <summary>
        /// Take damage from sprite
        /// </summary>
        /// <param name="sprite">Sprite to take damage from</param>
        public override void TakeDamage(Sprite sprite)
        {
            this.Kill();
        }

        /// <summary>
        /// Update the projectile
        /// </summary>
        /// <param name="gameTime">The GameTime object</param>
        public override void Update(GameTime gameTime)
        {
            if (this.accelerates)
            {
                this.Accelerate(gameTime);
            }
            else
            {
                this.Velocity = (this.DirectionVector * this.projectileVelocity * GameTimeHelper.GetTimeSinceLastTick(gameTime)) + this.inheritedvelocity;
            }

            base.Update(gameTime);
        }
    }
}