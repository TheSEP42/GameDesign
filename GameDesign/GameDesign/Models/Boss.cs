﻿using GameDesign.Helpers;
using GameDesign.Models.Weapons;
using GameDesign.Rules;
using Microsoft.Xna.Framework;

namespace GameDesign.Models
{
    /// <summary>
    /// Boss sprite
    /// </summary>
    internal class Boss : Enemy
    {
        /// <summary>
        /// Attack number
        /// </summary>
        private int attackNumber = 1;

        /// <summary>
        /// Time between attacks
        /// </summary>
        private float attackTime = 20;

        /// <summary>
        /// Time since last attack
        /// </summary>
        private float attackTimer = 15;

        /// <summary>
        /// weapon used
        /// </summary>
        private BossWeapon weapon = new BossWeapon(0, 0, 0);

        /// <summary>
        /// Initializes a new instance of the Boss class
        /// </summary>
        /// <param name="position"> Position of the boss</param>
        /// <param name="direction">Direction of the boss</param>
        public Boss(Vector2 position, float direction) : base(position, direction, 1f, 1f, 1, 0.6f)
        {
            this.Health = 10000;
        }

        /// <summary>
        /// Take damage
        /// </summary>
        /// <param name="sprite">Sprite to take damage from</param>
        public override void TakeDamage(Sprite sprite)
        {
            if (!(sprite is Player))
            {
                base.TakeDamage(sprite);
            }
        }

        /// <summary>
        /// Update the boss
        /// </summary>
        /// <param name="gameTime">The GameTime object</param>
        public override void Update(GameTime gameTime)
        {
            this.attackTimer += GameTimeHelper.GetTimeSinceLastTick(gameTime);
            base.Update(gameTime);
        }

        /// <summary>
        /// AI Logic
        /// </summary>
        /// <param name="gameTime">The GameTime object</param>
        protected override void AILogic(GameTime gameTime)
        {
            base.AILogic(gameTime);
            if (this.attackTimer > this.attackTime)
            {
                switch (this.attackNumber)
                {
                    case 1:
                        this.weapon.Shoot(this.Position, this.Angle, this.Velocity, BossWeapon.AttackType.Attack1);
                        this.attackTimer = 12;
                        break;

                    case 2:
                        this.weapon.Shoot(this.Position, this.Angle, this.Velocity, BossWeapon.AttackType.Attack2);
                        break;

                    case 3:
                        this.weapon.Shoot(this.Position, this.Angle, this.Velocity, BossWeapon.AttackType.Attack3);
                        break;

                    default:
                        this.weapon.Shoot(this.Position, this.Angle, this.Velocity, BossWeapon.AttackType.Attack1);
                        break;
                }

                this.attackTimer = 0;
                this.attackNumber++;
                if (this.attackNumber > 3)
                {
                    this.attackNumber = 1;
                }
            }
        }

        /// <summary>
        /// Animate boss death
        /// </summary>
        /// <param name="gameTime">The GameTime object</param>
        protected override void DeathAnimation(GameTime gameTime)
        {
            base.DeathAnimation(gameTime);
            if (this.deathAnimationTimer > this.deathAnimationLength)
            {
                GameRules.GameComplete();
            }
        }
    }
}