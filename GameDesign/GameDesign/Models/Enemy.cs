﻿using System;
using GameDesign.Content;
using GameDesign.Helpers;
using GameDesign.Managers;
using GameDesign.Models.Powerups;
using GameDesign.Rules;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;

namespace GameDesign.Models
{
    /// <summary>
    /// Enemy sprite
    /// </summary>
    internal class Enemy : Sprite
    {
        /// <summary>
        /// Can this sprite drop power ups
        /// </summary>
        public bool CanDropPowerup = true;

        /// <summary>
        /// Initial acceleration of the sprite
        /// </summary>
        public float InitialAcceleration = 0f;

        /// <summary>
        /// Initial acceleration timer
        /// </summary>
        public float InitialAccelerationTimer = 0f;

        /// <summary>
        /// Level of AI
        /// </summary>
        private int aiLevel;

        /// <summary>
        /// Is this sprite killed by the player
        /// </summary>
        private bool killedByPlayerCollision;

        /// <summary>
        /// Thruster sounds
        /// </summary>
        private Sound thrusters;

        /// <summary>
        /// Initializes a new instance of the <see cref="Enemy"/> class
        /// </summary>
        /// <param name="position">    Position of enemy</param>
        /// <param name="direction">   Direction of enemy</param>
        /// <param name="acceleration">Enemy acceleration</param>
        /// <param name="maxVelocity"> Maximum velocity of the enemy</param>
        /// <param name="aiDifficulty">Enemy difficulty</param>
        /// <param name="scale">       Scale of the enemy</param>
        public Enemy(Vector2 position, float direction, float acceleration, float maxVelocity, int aiDifficulty, float scale) :
            base(position, direction, scale)
        {
            this.LoadContent();
            this.Radius = 90 * scale;
            this.Health = 100;
            this.Damage = 200;
            this.rotationSpeed = MathHelper.ToRadians(180f);
            this.deathAnimationLength = 0.2f;
            this.DisplayInfo.Color = Color.Green;
            this.DisplayInfo.UnHealthyColor = Color.Red;
            this.acceleration = acceleration;
            this.maxVelocity = maxVelocity;
            this.aiLevel = aiDifficulty;
            this.canWrap = true;
            this.thrusters = new Sound(LoadedContent.GetSoundEffect("Thrusters"), 1f, 1f, 1f);
        }

        /// <summary>
        /// Gets or sets the lifespan of this sprite
        /// </summary>
        public float EnemyLifeSpan
        {
            get
            {
                return this.lifeSpan;
            }

            set
            {
                this.lifeSpan = value;
            }
        }

        /// <summary>
        /// Instantly delete the sprite with no death animation
        /// </summary>
        public override void InstantDelete()
        {
            this.thrusters.InstantStop();
            base.InstantDelete();
        }

        /// <summary>
        /// Load content required
        /// </summary>
        public void LoadContent()
        {
            this.DisplayInfo.Texture = LoadedContent.GetTexture("Enemy");
        }

        /// <summary>
        /// Stop noise made by this sprite
        /// </summary>
        public void StopNoise()
        {
            this.thrusters.InstantStop();
        }

        /// <summary>
        /// Take damage
        /// </summary>
        /// <param name="sprite">Sprite to be damaged by</param>
        public override void TakeDamage(Sprite sprite)
        {
            base.TakeDamage(sprite);
            if (this.IsDead && sprite is Player)
            {
                this.killedByPlayerCollision = true;
            }
            else
            {
                GameRules.AddPoints(sprite.Damage);
            }
        }

        /// <summary>
        /// Update the enemy
        /// </summary>
        /// <param name="gameTime">The GameTime object</param>
        public override void Update(GameTime gameTime)
        {
            if (!this.IsDead)
            {
                this.InitialAccelerationTimer += GameTimeHelper.GetTimeSinceLastTick(gameTime);
                this.AILogic(gameTime);
            }

            base.Update(gameTime);
        }

        /// <summary>
        /// AI Logic
        /// </summary>
        /// <param name="gameTime">The GameTime object</param>
        protected virtual void AILogic(GameTime gameTime)
        {
            if (this.InitialAccelerationTimer < this.InitialAcceleration)
            {
                this.Thrust(gameTime);
            }
            else if (!EntityManager.PlayerDead)
            {
                if (this.aiLevel > 0)
                {
                    Vector2 playerLocation = EntityManager.Player1.Position;
                    Vector2 offsetDirection = this.Position - playerLocation;

                    Vector3 facingDirection3 = new Vector3(this.DirectionVector.X, this.DirectionVector.Y, 0);
                    Vector3 offsetDirection3 = new Vector3(offsetDirection.X, offsetDirection.Y, 0);

                    float angleOffset = 0;

                    if (this.aiLevel == 2)
                    {
                        float distance = Vector2.Distance(playerLocation, this.Position);
                        if (distance > 30)
                        {
                            playerLocation = EntityManager.Player1.Position + (EntityManager.Player1.Velocity * 60);
                            offsetDirection = this.Position - playerLocation;

                            facingDirection3 = new Vector3(this.DirectionVector.X, this.DirectionVector.Y, 0);
                            offsetDirection3 = new Vector3(offsetDirection.X, offsetDirection.Y, 0);
                        }
                    }

                    Vector3 cross = Vector3.Cross(facingDirection3, offsetDirection3);

                    if (cross.Z + angleOffset > 0)
                    {
                        this.RotateAntiClockwise(gameTime, this.rotationSpeed);
                    }
                    else if (cross.Z + angleOffset < 0)
                    {
                        this.RotateClockwise(gameTime, this.rotationSpeed);
                    }

                    if (Math.Abs(cross.Z + angleOffset) < 30)
                    {
                        this.Thrust(gameTime);
                    }
                }
                else
                {
                    this.Thrust(gameTime);
                }
            }
            else
            {
                this.thrusters.Stop(gameTime);
            }
        }

        /// <summary>
        /// Animate the death of this enemy
        /// </summary>
        /// <param name="gameTime">The GameTime object</param>
        protected override void DeathAnimation(GameTime gameTime)
        {
            base.DeathAnimation(gameTime);
            this.thrusters.Stop(gameTime);
            float currentTime = GameTimeHelper.GetCurrentGameTime(gameTime);
            this.RotateAntiClockwise(gameTime, MathHelper.ToRadians(720));
            this.DisplayInfo.Scale = (1 - this.DeathAnimationPercent) * this.DisplayInfo.InitialScale;

            if (this.deathAnimationTimer > this.deathAnimationLength)
            {
                this.Explosion(gameTime);
                this.thrusters.InstantStop();

                if (!this.killedByPlayerCollision)
                {
                    GameRules.AddPoints(100);
                    this.DropPowerup();
                }
                else
                {
                    GameRules.AddPoints(20);
                }
            }
        }

        /// <summary>
        /// Kill this enemy
        /// </summary>
        protected override void Kill()
        {
            base.Kill();
        }

        /// <summary>
        /// Drop power up
        /// </summary>
        private void DropPowerup()
        {
            if (this.CanDropPowerup)
            {
                PowerupType? powerup = GameRules.GeneratePowerupType();
                if (powerup.HasValue)
                {
                    switch (powerup)
                    {
                        case PowerupType.Health:
                            EntityManager.Add(new HealthPack(this.Position, 0));
                            break;

                        case PowerupType.Shield:
                            EntityManager.Add(new Shield(this.Position, 0));
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// Generate explosion on death
        /// </summary>
        /// <param name="gameTime">The GameTime object</param>
        private void Explosion(GameTime gameTime)
        {
            this.CreateParticleEffects(gameTime, 1000, 359, 1f, Color.Red, 20, 20);
            Random rand = new Random();
            double chanceResult = rand.NextDouble();
            SoundEffectInstance explosion;
            if (chanceResult < (1f / 3f))
            {
                explosion = LoadedContent.GetSoundEffect("Explosion1");
                explosion.Volume = 0.5f;
            }
            else if (chanceResult < ((1f / 3f) * 2f))
            {
                explosion = LoadedContent.GetSoundEffect("Explosion2");
                explosion.Volume = 0.2f;
            }
            else
            {
                explosion = LoadedContent.GetSoundEffect("Explosion3");
                explosion.Volume = 0.2f;
            }

            explosion.Play();
        }

        /// <summary>
        /// Accelerate and animate exhaust
        /// </summary>
        /// <param name="gameTime">The GameTime object</param>
        private void Thrust(GameTime gameTime)
        {
            this.Accelerate(gameTime);
            this.thrusters.Start(gameTime);
            this.CreateParticleEffects(gameTime, 500, 70, 1f, this.DisplayInfo.CurrentColor, 100f, 20);
        }
    }
}