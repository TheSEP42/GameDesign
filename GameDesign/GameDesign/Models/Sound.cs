﻿using GameDesign.Helpers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;

namespace GameDesign.Models
{
    /// <summary>
    /// Sounds effect
    /// </summary>
    public class Sound
    {
        /// <summary>
        /// Time to fade in
        /// </summary>
        private float fadeInTime;

        /// <summary>
        /// Time to fade out
        /// </summary>
        private float fadeOutTime;

        /// <summary>
        /// Maximum volume
        /// </summary>
        private float maxVolume;

        /// <summary>
        /// Sound effect
        /// </summary>
        private SoundEffectInstance soundEffect;

        /// <summary>
        /// Is this sound stopped
        /// </summary>
        private bool stopped;

        /// <summary>
        /// Initializes a new instance of the Sound class
        /// </summary>
        /// <param name="sound">      Sound to play</param>
        /// <param name="maxVolume">  Maximum volume</param>
        /// <param name="fadeOutTime">Time to fade out</param>
        /// <param name="fadeInTime"> Time to fade in</param>
        public Sound(SoundEffectInstance sound, float maxVolume, float fadeOutTime, float fadeInTime) : this(sound, maxVolume)
        {
            this.fadeOutTime = fadeOutTime;
            this.fadeInTime = fadeInTime;
        }

        /// <summary>
        /// Initializes a new instance of the Sound class
        /// </summary>
        /// <param name="sound">    Sound to play</param>
        /// <param name="maxVolume">Maximum volume</param>
        public Sound(SoundEffectInstance sound, float maxVolume)
        {
            this.soundEffect = sound;
            this.stopped = true;
            this.maxVolume = maxVolume;
            this.soundEffect.Volume = 0;
        }

        /// <summary>
        /// Instantly stop the sound
        /// </summary>
        public void InstantStop()
        {
            this.soundEffect.Stop();
        }

        /// <summary>
        /// Start playing the sound while fading in
        /// </summary>
        /// <param name="gameTime">The GameTime object</param>
        public void Start(GameTime gameTime)
        {
            if (this.soundEffect.State != SoundState.Playing)
            {
                this.soundEffect.Play();
            }

            float tickTime = GameTimeHelper.GetTimeSinceLastTick(gameTime);
            float newVolume = this.soundEffect.Volume + ((tickTime / this.fadeOutTime) * this.maxVolume);

            if (newVolume > this.maxVolume)
            {
                newVolume = this.maxVolume;
            }
            else if (newVolume < 0)
            {
                newVolume = 0;
            }

            this.soundEffect.Volume = newVolume;

            this.stopped = false;
        }

        /// <summary>
        /// Stop playing the sound after fading out
        /// </summary>
        /// <param name="gameTime">The GameTime object</param>
        public void Stop(GameTime gameTime)
        {
            if (!this.stopped)
            {
                if (this.soundEffect.State != SoundState.Playing)
                {
                    this.soundEffect.Play();
                }

                float tickTime = GameTimeHelper.GetTimeSinceLastTick(gameTime);
                float newVolume = this.soundEffect.Volume - ((tickTime / this.fadeOutTime) * this.maxVolume);

                if (newVolume >= this.maxVolume)
                {
                    newVolume = this.maxVolume;
                }
                else if (newVolume <= 0)
                {
                    newVolume = 0;
                    this.soundEffect.Stop();
                    this.stopped = true;
                }

                this.soundEffect.Volume = newVolume;
            }
        }
    }
}