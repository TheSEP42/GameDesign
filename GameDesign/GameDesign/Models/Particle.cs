﻿using GameDesign.Content;
using GameDesign.Helpers;
using GameDesign.Rules;
using Microsoft.Xna.Framework;

namespace GameDesign.Models
{
    /// <summary>
    /// Particle sprite
    /// </summary>
    internal class Particle : Sprite
    {
        /// <summary>
        /// Velocity inherited by creator
        /// </summary>
        private Vector2 inheritedvelocity;

        /// <summary>
        /// velocity of the particle
        /// </summary>
        private float velocity;

        /// <summary>
        /// Initializes a new instance of the Particle class
        /// </summary>
        /// <param name="position">         Position of the particle</param>
        /// <param name="direction">        Direction of the particle</param>
        /// <param name="inheritedVelocity">Velocity inherited from creator</param>
        /// <param name="velocity">         Velocity of the particle</param>
        /// <param name="color">            Color of the particle</param>
        /// <param name="lifeSpan">         Life span of the particle</param>
        public Particle(Vector2 position, float direction, Vector2 inheritedVelocity, float velocity, Color color, float lifeSpan) :
            base(position, direction)
        {
            this.inheritedvelocity = inheritedVelocity;
            this.DisplayInfo.Texture = LoadedContent.GetTexture("Particle");
            this.DisplayInfo.Color = color;
            this.velocity = velocity;
            this.lifeSpan = lifeSpan;
            this.canWrap = true;
        }

        /// <summary>
        /// Update the particle
        /// </summary>
        /// <param name="gameTime">The GameTime object</param>
        public override void Update(GameTime gameTime)
        {
            this.Velocity = (this.DirectionVector * this.velocity * GameTimeHelper.GetTimeSinceLastTick(gameTime)) + this.inheritedvelocity;
            base.Update(gameTime);
            this.DisplayInfo.CurrentColor *= 1 - this.GetLifePercentage(gameTime);
        }
    }
}