﻿using System;
using GameDesign.Content;
using GameDesign.Models.Projectiles;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;

namespace GameDesign.Models.Weapons
{
    /// <summary>
    /// Double gun weapon
    /// </summary>
    internal class DoubleGun : Weapon
    {
        /// <summary>
        /// Initializes a new instance of the DoubleGun class
        /// </summary>
        /// <param name="baseAccuracy">            Base accuracy</param>
        /// <param name="baseProjectilesPerSecond">base projectiles per second</param>
        /// <param name="baseDamage">              Base damage</param>
        public DoubleGun(int baseAccuracy, float baseProjectilesPerSecond, int baseDamage) : base(baseAccuracy, baseProjectilesPerSecond, baseDamage)
        {
            this.Type = WeaponType.DoubleGun;
            this.guiImage = LoadedContent.GetTexture("DoubleGunOSD");
        }

        /// <summary>
        /// Generate projectiles
        /// </summary>
        /// <param name="position"> Projectile position</param>
        /// <param name="direction">projectile direction</param>
        /// <param name="velocity"> projectiles velocity</param>
        protected override void GenerateProjectiles(Vector2 position, float direction, Vector2 velocity)
        {
            if (this.timer > 1 / this.projectilesPerSecond)
            {
                Random random = new Random();
                float leftBulletAngleChange = direction - MathHelper.ToRadians(90);
                float rightBulletAngleChange = direction + MathHelper.ToRadians(90);

                Vector2 leftVectorChange = new Vector2(
                    (float)Math.Sin(leftBulletAngleChange),
                    -(float)Math.Cos(leftBulletAngleChange));
                Vector2 rightVectorChange = new Vector2(
                    (float)Math.Sin(rightBulletAngleChange),
                    -(float)Math.Cos(rightBulletAngleChange));

                int offset = 15;
                leftVectorChange *= offset;
                rightVectorChange *= offset;

                float leftBulletDirection = direction + MathHelper.ToRadians(((float)random.NextDouble() * this.accuracy) - (this.accuracy / 2));
                float rightBulletDirection = direction + MathHelper.ToRadians(((float)random.NextDouble() * this.accuracy) - (this.accuracy / 2));

                Bullet leftBullet = new Bullet(position + leftVectorChange, leftBulletDirection, velocity);
                Bullet rightBullet = new Bullet(position + rightVectorChange, rightBulletDirection, velocity);

                leftBullet.Damage = this.damage;
                rightBullet.Damage = this.damage;

                leftBullet.DisplayInfo.Color = Color.Red;
                rightBullet.DisplayInfo.Color = Color.Red;

                SoundEffectInstance gunShot = LoadedContent.GetSoundEffect("Laser");
                gunShot.Volume = 0.1f;
                gunShot.Play();

                this.AddProjectile(leftBullet);
                this.AddProjectile(rightBullet);
                this.timer = 0;
            }
        }

        /// <summary>
        /// Weapon level 0
        /// </summary>
        protected override void WeaponLevel0()
        {
        }

        /// <summary>
        /// Weapon level 1
        /// </summary>
        protected override void WeaponLevel1()
        {
            this.WeaponLevel0();
            this.accuracy = this.baseAccuracy -= Convert.ToInt32(this.baseAccuracy * 0.5);
        }

        /// <summary>
        /// Weapon level 2
        /// </summary>
        protected override void WeaponLevel2()
        {
            this.WeaponLevel1();
            this.damage = this.baseDamage + Convert.ToInt32(this.baseDamage * 0.5);
        }

        /// <summary>
        /// Weapon level 3
        /// </summary>
        protected override void WeaponLevel3()
        {
            this.WeaponLevel2();
            this.projectilesPerSecond = this.baseProjectilesPerSecond + Convert.ToInt32(this.baseProjectilesPerSecond * 0.5);
        }
    }
}