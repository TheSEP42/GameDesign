﻿using GameDesign.Helpers;
using GameDesign.Managers;
using GameDesign.Models.Projectiles;
using GameDesign.Rules;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameDesign.Models.Weapons
{
    /// <summary>
    /// Weapons class
    /// </summary>
    internal abstract class Weapon
    {
        /// <summary>
        /// Accuracy of the weapon
        /// </summary>
        protected int accuracy;

        /// <summary>
        /// Base accuracy of the weapon
        /// </summary>
        protected int baseAccuracy;

        /// <summary>
        /// Base damage of the weapon
        /// </summary>
        protected int baseDamage;

        /// <summary>
        /// Base projectiles per second
        /// </summary>
        protected float baseProjectilesPerSecond;

        /// <summary>
        /// Damage of the weapon
        /// </summary>
        protected int damage;

        /// <summary>
        /// The on screen display image
        /// </summary>
        protected Texture2D guiImage;

        /// <summary>
        /// Projectiles generated per second
        /// </summary>
        protected float projectilesPerSecond;

        /// <summary>
        /// Timer for tracking projectiles per second
        /// </summary>
        protected float timer = 0;

        /// <summary>
        /// Initializes a new instance of the Weapon class
        /// </summary>
        /// <param name="baseAccuracy">            Base accuracy</param>
        /// <param name="baseProjectilesPerSecond">base projectiles per second</param>
        /// <param name="baseDamage">              Base damage</param>
        public Weapon(int baseAccuracy, float baseProjectilesPerSecond, int baseDamage)
        {
            this.baseAccuracy = baseAccuracy;
            this.baseDamage = baseDamage;
            this.baseProjectilesPerSecond = baseProjectilesPerSecond;
            this.accuracy = baseAccuracy;
            this.damage = baseDamage;
            this.projectilesPerSecond = baseProjectilesPerSecond;
        }

        /// <summary>
        /// Gets the image for the on screen display
        /// </summary>
        public Texture2D OSDImage
        {
            get
            {
                return this.guiImage;
            }
        }

        /// <summary>
        /// Gets or sets the type of weapon
        /// </summary>
        public WeaponType Type
        {
            get; protected set;
        }

        /// <summary>
        /// Shoot this weapon
        /// </summary>
        /// <param name="position"> Position shooting from</param>
        /// <param name="direction">Direction shooting from</param>
        /// <param name="velocity"> Velocity of creator</param>
        public virtual void Shoot(Vector2 position, float direction, Vector2 velocity)
        {
            int upgradeLevel = GameRules.WeaponUpgradeLevel(this.Type);
            switch (upgradeLevel)
            {
                case 0:
                    this.WeaponLevel0();
                    break;

                case 1:
                    this.WeaponLevel1();
                    break;

                case 2:
                    this.WeaponLevel2();
                    break;

                case 3:
                    this.WeaponLevel3();
                    break;

                default:
                    this.WeaponLevel0();
                    break;
            }

            this.GenerateProjectiles(position, direction, velocity);
        }

        /// <summary>
        /// Update this weapon
        /// </summary>
        /// <param name="gameTime">The GameTime object</param>
        public void Update(GameTime gameTime)
        {
            this.timer += GameTimeHelper.GetTimeSinceLastTick(gameTime);
        }

        /// <summary>
        /// Add projectile to entity manager
        /// </summary>
        /// <param name="projectile">Projectile to add</param>
        protected void AddProjectile(Projectile projectile)
        {
            EntityManager.Add(projectile);
        }

        /// <summary>
        /// Generate projectiles
        /// </summary>
        /// <param name="position"> Projectile position</param>
        /// <param name="direction">projectile direction</param>
        /// <param name="velocity"> projectiles velocity</param>
        protected abstract void GenerateProjectiles(Vector2 position, float direction, Vector2 velocity);

        /// <summary>
        /// Weapon level 0
        /// </summary>
        protected abstract void WeaponLevel0();

        /// <summary>
        /// Weapon level 1
        /// </summary>
        protected abstract void WeaponLevel1();

        /// <summary>
        /// Weapon level 2
        /// </summary>
        protected abstract void WeaponLevel2();

        /// <summary>
        /// Weapon level 3
        /// </summary>
        protected abstract void WeaponLevel3();
    }
}