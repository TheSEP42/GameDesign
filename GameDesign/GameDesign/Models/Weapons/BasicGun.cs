﻿using System;
using GameDesign.Content;
using GameDesign.Models.Projectiles;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;

namespace GameDesign.Models.Weapons
{
    /// <summary>
    /// Basic gun weapon
    /// </summary>
    internal class BasicGun : Weapon
    {
        /// <summary>
        /// Initializes a new instance of the BasicGun class
        /// </summary>
        /// <param name="baseAccuracy">            Base accuracy</param>
        /// <param name="baseProjectilesPerSecond">base projectiles per second</param>
        /// <param name="baseDamage">              Base damage</param>
        public BasicGun(int baseAccuracy, float baseProjectilesPerSecond, int baseDamage) : base(baseAccuracy, baseProjectilesPerSecond, baseDamage)
        {
            this.Type = WeaponType.BasicGun;
            this.guiImage = LoadedContent.GetTexture("BasicGunOSD");
        }

        /// <summary>
        /// Generate projectiles
        /// </summary>
        /// <param name="position"> Projectile position</param>
        /// <param name="direction">projectile direction</param>
        /// <param name="velocity"> projectiles velocity</param>
        protected override void GenerateProjectiles(Vector2 position, float direction, Vector2 velocity)
        {
            if (this.timer > 1 / this.projectilesPerSecond)
            {
                Random random = new Random();
                direction += MathHelper.ToRadians(((float)random.NextDouble() * this.accuracy) - (this.accuracy / 2));

                Bullet bullet = new Bullet(position, direction, velocity);
                bullet.DisplayInfo.Color = Color.Red;
                bullet.Damage = this.damage;

                SoundEffectInstance gunShot = LoadedContent.GetSoundEffect("Laser");
                gunShot.Volume = 0.1f;
                gunShot.Play();

                this.AddProjectile(bullet);
                this.timer = 0;
            }
        }

        /// <summary>
        /// Weapon level 0
        /// </summary>
        protected override void WeaponLevel0()
        {
        }

        /// <summary>
        /// Weapon level 1
        /// </summary>
        protected override void WeaponLevel1()
        {
            this.WeaponLevel0();
            this.accuracy = this.baseAccuracy -= Convert.ToInt32(this.baseAccuracy * 0.5);
        }

        /// <summary>
        /// Weapon level 2
        /// </summary>
        protected override void WeaponLevel2()
        {
            this.WeaponLevel1();
            this.damage = this.baseDamage + Convert.ToInt32(this.baseDamage * 0.5);
        }

        /// <summary>
        /// Weapon level 3
        /// </summary>
        protected override void WeaponLevel3()
        {
            this.WeaponLevel2();
            this.projectilesPerSecond = this.baseProjectilesPerSecond + Convert.ToInt32(this.baseProjectilesPerSecond * 0.5);
        }
    }
}