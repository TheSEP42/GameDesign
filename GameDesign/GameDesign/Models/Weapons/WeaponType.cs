﻿namespace GameDesign.Models.Weapons
{
    /// <summary>
    /// Type of weapon
    /// </summary>
    public enum WeaponType
    {
        /// <summary>
        /// Basic single stream weapon
        /// </summary>
        BasicGun,

        /// <summary>
        /// Rocket gun
        /// </summary>
        RocketGun,

        /// <summary>
        /// Dual stream gun
        /// </summary>
        DoubleGun
    }
}