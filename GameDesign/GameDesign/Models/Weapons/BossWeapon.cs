﻿using GameDesign.Managers;
using Microsoft.Xna.Framework;

namespace GameDesign.Models.Weapons
{
    /// <summary>
    /// Weapon the final boss uses
    /// </summary>
    internal class BossWeapon : Weapon
    {
        /// <summary>
        /// Initializes a new instance of the BossWeapon class
        /// </summary>
        /// <param name="baseAccuracy">            Base accuracy</param>
        /// <param name="baseProjectilesPerSecond">base projectiles per second</param>
        /// <param name="baseDamage">              Base damage</param>
        public BossWeapon(int baseAccuracy, float baseProjectilesPerSecond, int baseDamage) :
            base(baseAccuracy, baseProjectilesPerSecond, baseDamage)
        {
        }

        /// <summary>
        /// Attack types
        /// </summary>
        public enum AttackType
        {
            /// <summary>
            /// First attack
            /// </summary>
            Attack1,

            /// <summary>
            /// Second attack
            /// </summary>
            Attack2,

            /// <summary>
            /// Third attack
            /// </summary>
            Attack3
        }

        /// <summary>
        /// Shoot this weapon
        /// </summary>
        /// <param name="position"> Position shooting from</param>
        /// <param name="direction">Direction shooting from</param>
        /// <param name="velocity"> Velocity of creator</param>
        public override void Shoot(Vector2 position, float direction, Vector2 velocity)
        {
            this.Shoot(position, direction, velocity, AttackType.Attack1);
        }

        /// <summary>
        /// Shoot this weapon
        /// </summary>
        /// <param name="position">  Position shooting from</param>
        /// <param name="direction"> Direction shooting from</param>
        /// <param name="velocity">  Velocity of creator</param>
        /// <param name="attackType">Attack type</param>
        public void Shoot(Vector2 position, float direction, Vector2 velocity, AttackType attackType)
        {
            switch (attackType)
            {
                case AttackType.Attack1:
                    this.Attack1(position, direction, velocity);
                    break;

                case AttackType.Attack2:
                    this.Attack2(position, direction, velocity);
                    break;

                case AttackType.Attack3:
                    this.Attack3(position, direction, velocity);
                    break;

                default:
                    this.Attack1(position, direction, velocity);
                    break;
            }
        }

        /// <summary>
        /// Necessary method
        /// </summary>
        /// <param name="position"> The parameter is not used.</param>
        /// <param name="direction">The parameter is not used.</param>
        /// <param name="velocity"> The parameter is not used.</param>
        protected override void GenerateProjectiles(Vector2 position, float direction, Vector2 velocity)
        {
            // Does nothing
        }

        /// <summary>
        /// Necessary method
        /// </summary>
        protected override void WeaponLevel0()
        {
            // Does nothing
        }

        /// <summary>
        /// Necessary method
        /// </summary>
        protected override void WeaponLevel1()
        {
            // Does nothing
        }

        /// <summary>
        /// Necessary method
        /// </summary>
        protected override void WeaponLevel2()
        {
            // Does nothing
        }

        /// <summary>
        /// Necessary method
        /// </summary>
        protected override void WeaponLevel3()
        {
            // Does nothing
        }

        /// <summary>
        /// First attack
        /// </summary>
        /// <param name="position"> Position of creator</param>
        /// <param name="direction">Direction of creator</param>
        /// <param name="velocity"> Velocity of creator</param>
        private void Attack1(Vector2 position, float direction, Vector2 velocity)
        {
            int degrees = 180;
            for (int i = -(degrees / 2); i < (degrees / 2); i += 10)
            {
                float offSet = MathHelper.ToRadians(i);
                Enemy littleEnemy = new Enemy(position, direction + offSet, 5f, 5f, 0, 0.075f);
                littleEnemy.EnemyLifeSpan = 3f;
                littleEnemy.DisplayInfo.Color = Color.Cyan;
                littleEnemy.CanDropPowerup = false;
                littleEnemy.InitialAcceleration = 3f;
                EntityManager.Add(littleEnemy);
            }
        }

        /// <summary>
        /// Second attack
        /// </summary>
        /// <param name="position"> Position of creator</param>
        /// <param name="direction">Direction of creator</param>
        /// <param name="velocity"> Velocity of creator</param>
        private void Attack2(Vector2 position, float direction, Vector2 velocity)
        {
            int degrees = 360;
            for (int i = -(degrees / 2); i < (degrees / 2); i += 40)
            {
                float offSet = MathHelper.ToRadians(i);
                Enemy enemy = new Enemy(position, direction + offSet, 5f, 4f, 1, 0.15f);
                enemy.InitialAcceleration = 1.5f;
                EntityManager.Add(enemy);
            }
        }

        /// <summary>
        /// Third attack
        /// </summary>
        /// <param name="position"> Position of creator</param>
        /// <param name="direction">Direction of creator</param>
        /// <param name="velocity"> Velocity of creator</param>
        private void Attack3(Vector2 position, float direction, Vector2 velocity)
        {
            int degrees = 360;
            for (int i = -(degrees / 2); i < (degrees / 2); i += 60)
            {
                float offSet = MathHelper.ToRadians(i);
                Enemy enemy = new Enemy(position, direction + offSet, 5f, 5f, 2, 0.2f);
                enemy.DisplayInfo.Color = Color.Yellow;
                enemy.InitialAcceleration = 1.5f;
                EntityManager.Add(enemy);
            }
        }
    }
}