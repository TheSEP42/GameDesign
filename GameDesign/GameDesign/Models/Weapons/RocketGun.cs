﻿using System;
using GameDesign.Content;
using GameDesign.Models.Projectiles;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;

namespace GameDesign.Models.Weapons
{
    /// <summary>
    /// rocket gun weapon
    /// </summary>
    internal class RocketGun : Weapon
    {
        /// <summary>
        /// Base blast radius
        /// </summary>
        private float baseBlastRadius;

        /// <summary>
        /// Blast radius
        /// </summary>
        private float blastRadius;

        /// <summary>
        /// Initializes a new instance of the RocketGun class
        /// </summary>
        /// <param name="baseAccuracy">            Base accuracy</param>
        /// <param name="baseProjectilesPerSecond">base projectiles per second</param>
        /// <param name="baseDamage">              Base damage</param>
        /// <param name="baseBlastRadius">         Base blast radius</param>
        public RocketGun(int baseAccuracy, float baseProjectilesPerSecond, int baseDamage, float baseBlastRadius) : base(baseAccuracy, baseProjectilesPerSecond, baseDamage)
        {
            this.Type = WeaponType.RocketGun;
            this.guiImage = LoadedContent.GetTexture("RocketOSD");
            this.baseBlastRadius = baseBlastRadius;
            this.blastRadius = baseBlastRadius;
        }

        /// <summary>
        /// Generate projectiles
        /// </summary>
        /// <param name="position"> Projectile position</param>
        /// <param name="direction">projectile direction</param>
        /// <param name="velocity"> projectiles velocity</param>
        protected override void GenerateProjectiles(Vector2 position, float direction, Vector2 velocity)
        {
            if (this.timer > 1 / this.projectilesPerSecond)
            {
                Random random = new Random();
                direction += MathHelper.ToRadians(((float)random.NextDouble() * this.accuracy) - (this.accuracy / 2));
                Rocket rocket = new Rocket(position, direction, velocity);
                rocket.Radius = this.blastRadius;
                rocket.Damage = this.damage;
                this.AddProjectile(rocket);
                SoundEffectInstance gunShot = LoadedContent.GetSoundEffect("Rocket");
                gunShot.Volume = 0.1f;
                gunShot.Play();
                this.timer = 0;
            }
        }

        /// <summary>
        /// Weapon level 0
        /// </summary>
        protected override void WeaponLevel0()
        {
        }

        /// <summary>
        /// Weapon level 1
        /// </summary>
        protected override void WeaponLevel1()
        {
            this.WeaponLevel0();
            this.blastRadius = this.baseBlastRadius * 2;
        }

        /// <summary>
        /// Weapon level 2
        /// </summary>
        protected override void WeaponLevel2()
        {
            this.WeaponLevel1();
            this.damage = this.baseDamage + Convert.ToInt32(this.baseDamage * 0.5);
        }

        /// <summary>
        /// Weapon level 3
        /// </summary>
        protected override void WeaponLevel3()
        {
            this.WeaponLevel2();
            this.projectilesPerSecond = this.baseProjectilesPerSecond + Convert.ToInt32(this.baseProjectilesPerSecond * 0.5);
        }
    }
}