﻿using System;
using System.Collections.Generic;
using System.Linq;
using GameDesign.Content;
using GameDesign.Helpers;
using GameDesign.Models.Projectiles;
using GameDesign.Models.Weapons;
using GameDesign.Rules;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace GameDesign.Models
{
    /// <summary>
    /// Player sprite
    /// </summary>
    internal class Player : Sprite
    {
        /// <summary>
        /// Aiming direction
        /// </summary>
        private float aimDirection = 0;

        /// <summary>
        /// Is the next weapon button pressed
        /// </summary>
        private bool nextWeaponPressed = false;

        /// <summary>
        /// Is the previous weapon button pressed
        /// </summary>
        private bool previousWeaponPressed = false;

        /// <summary>
        /// Thruster sound
        /// </summary>
        private Sound thrusters;

        /// <summary>
        /// Active weapon
        /// </summary>
        private Weapon weapon;

        /// <summary>
        /// Initializes a new instance of the Player class
        /// </summary>
        /// <param name="position"> Position of the player</param>
        /// <param name="direction">Direction of the player</param>
        public Player(Vector2 position, float direction) : base(position, direction, 0.7f)
        {
            this.rotationSpeed = MathHelper.ToRadians(360f);
            this.Radius = 20;
            this.Health = 1000;
            this.Damage = 1000;
            this.DisplayInfo.Color = Color.White;
            this.DisplayInfo.UnHealthyColor = Color.Red;
            this.weapon = GameRules.GetPlayerWeapon(WeaponType.BasicGun);
            this.canWrap = true;
            this.thrusters = new Sound(LoadedContent.GetSoundEffect("Thrusters"), 1f, 1f, 1f);
            this.LoadContent();
        }

        /// <summary>
        /// Gets the GUI image for the current weapon
        /// </summary>
        public Texture2D GUIWeaponImage
        {
            get { return this.weapon.OSDImage; }
        }

        /// <summary>
        /// Draw the player
        /// </summary>
        /// <param name="spriteBatch">The SpriteBatch object</param>
        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
        }

        /// <summary>
        /// Instantly delete the player
        /// </summary>
        public override void InstantDelete()
        {
            this.thrusters.InstantStop();
            base.InstantDelete();
        }

        /// <summary>
        /// Load content needed
        /// </summary>
        public void LoadContent()
        {
            this.DisplayInfo.Texture = LoadedContent.GetTexture("Player");
        }

        /// <summary>
        /// Stop noise
        /// </summary>
        public void StopNoise()
        {
            this.thrusters.InstantStop();
        }

        /// <summary>
        /// Update the player
        /// </summary>
        /// <param name="gameTime">The GameTime object</param>
        public override void Update(GameTime gameTime)
        {
            List<Projectile> projectiles = new List<Projectile>();
            this.weapon.Update(gameTime);
            if (Keyboard.GetState().IsKeyDown(Keys.W))
            {
                this.Accelerate(gameTime);
                this.CreateParticleEffects(gameTime, 500, 70, 1, this.DisplayInfo.CurrentColor, 100f, 20);
                this.thrusters.Start(gameTime);
            }
            else
            {
                this.thrusters.Stop(gameTime);
            }

            if (Keyboard.GetState().IsKeyDown(Keys.A))
            {
                this.RotateAntiClockwise(gameTime, this.rotationSpeed);
            }

            if (Keyboard.GetState().IsKeyDown(Keys.D))
            {
                this.RotateClockwise(gameTime, this.rotationSpeed);
            }

            if (Mouse.GetState().LeftButton == ButtonState.Pressed)
            {
                this.Shoot(gameTime);
            }

            if (Keyboard.GetState().IsKeyDown(Keys.Q))
            {
                if (!this.previousWeaponPressed)
                {
                    this.PreviousWeapon();
                    this.previousWeaponPressed = true;
                }
            }
            else
            {
                this.previousWeaponPressed = false;
            }

            if (Keyboard.GetState().IsKeyDown(Keys.E))
            {
                if (!this.nextWeaponPressed)
                {
                    this.NextWeapon();
                    this.nextWeaponPressed = true;
                }
            }
            else
            {
                this.nextWeaponPressed = false;
            }

            MouseState mouseState = Mouse.GetState();
            Vector2 mousePosition = new Vector2(mouseState.X, mouseState.Y);
            Vector2 aimVector = mousePosition - this.Position;
            this.aimDirection = AngleHelper.VectorToRadians(aimVector);

            base.Update(gameTime);
        }

        /// <summary>
        /// Animate the death of the player
        /// </summary>
        /// <param name="gameTime">The GameTime object</param>
        protected override void DeathAnimation(GameTime gameTime)
        {
            this.thrusters.Stop(gameTime);
            base.DeathAnimation(gameTime);
        }

        /// <summary>
        /// Rotate to the next weapon
        /// </summary>
        private void NextWeapon()
        {
            int currentWeapon = (int)this.weapon.Type;
            int maxWeapnValue = (int)Enum.GetValues(typeof(WeaponType)).Cast<WeaponType>().Last();
            int minWeapnValue = (int)Enum.GetValues(typeof(WeaponType)).Cast<WeaponType>().First();
            currentWeapon++;
            if (currentWeapon > maxWeapnValue)
            {
                currentWeapon = minWeapnValue;
            }

            WeaponType newWeapon = (WeaponType)currentWeapon;

            this.weapon = GameRules.GetPlayerWeapon(newWeapon);
        }

        /// <summary>
        /// Rotate to the previous weapon
        /// </summary>
        private void PreviousWeapon()
        {
            int currentWeapon = (int)this.weapon.Type;
            int maxWeaponValue = (int)Enum.GetValues(typeof(WeaponType)).Cast<WeaponType>().Last();
            int minWeaponValue = (int)Enum.GetValues(typeof(WeaponType)).Cast<WeaponType>().First();
            currentWeapon--;
            if (currentWeapon < minWeaponValue)
            {
                currentWeapon = maxWeaponValue;
            }

            WeaponType newWeapon = (WeaponType)currentWeapon;

            this.weapon = GameRules.GetPlayerWeapon(newWeapon);
        }

        /// <summary>
        /// Shoot the players active weapon
        /// </summary>
        /// <param name="gameTime">The GameTime object</param>
        private void Shoot(GameTime gameTime)
        {
            Vector2 initialPosition = this.Position + (this.DirectionVector * this.Radius);
            this.weapon.Shoot(this.Position, this.aimDirection, this.Velocity);
        }
    }
}