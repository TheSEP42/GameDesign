﻿using System;
using System.Collections.Generic;
using GameDesign.Content;
using GameDesign.Helpers;
using GameDesign.Managers;
using GameDesign.Menus;
using GameDesign.Menus.Buttons;
using GameDesign.Models;
using GameDesign.Models.Powerups;
using GameDesign.Models.Weapons;
using GameDesign.Rules;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace GameDesign
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game : Microsoft.Xna.Framework.Game
    {
        /// <summary>
        /// The back story menu
        /// </summary>
        private Menu backStoryMenu;

        /// <summary>
        /// The boss level
        /// </summary>
        private BossLevel bossLevel;

        /// <summary>
        /// The controls menu
        /// </summary>
        private Menu controlsMenu;

        /// <summary>
        /// The GraphicsDeviceManager
        /// </summary>
        private GraphicsDeviceManager graphics;

        /// <summary>
        /// The main level
        /// </summary>
        private Level level;

        /// <summary>
        /// The lose menu
        /// </summary>
        private Menu loseMenu;

        /// <summary>
        /// The main menu
        /// </summary>
        private Menu mainMenu;

        /// <summary>
        /// The pause menu
        /// </summary>
        private Menu pauseMenu;

        /// <summary>
        /// State when quit to main menu
        /// </summary>
        private GameState? quitToMainMenuState;

        /// <summary>
        /// The SpriteBatch object
        /// </summary>
        private SpriteBatch spriteBatch;

        /// <summary>
        /// The upgrade menu
        /// </summary>
        private Menu upgradeMenu;

        /// <summary>
        /// The win menu
        /// </summary>
        private Menu winMenu;

        /// <summary>
        /// Initializes a new instance of the Game class
        /// </summary>
        public Game()
        {
            this.graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            switch (GameRules.State)
            {
                case GameState.Playing:
                    this.level.Draw(this.spriteBatch);
                    break;

                case GameState.UpgradeMenu:
                    this.upgradeMenu.Draw(this.spriteBatch);
                    break;

                case GameState.BossLevel:
                    this.bossLevel.Draw(this.spriteBatch);
                    break;

                case GameState.GameComplete:
                    this.winMenu.Draw(this.spriteBatch);
                    break;

                case GameState.GameOver:
                    this.loseMenu.Draw(this.spriteBatch);
                    break;

                case GameState.Paused:
                    this.pauseMenu.Draw(this.spriteBatch);
                    break;

                case GameState.Controls:
                    this.controlsMenu.Draw(this.spriteBatch);
                    break;

                case GameState.BackStory:
                    this.backStoryMenu.Draw(this.spriteBatch);
                    break;

                case GameState.MainMenu:
                    this.mainMenu.Draw(this.spriteBatch);
                    break;
            }

            base.Draw(gameTime);
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run. This is
        /// where it can query for any required services and load any non-graphic related content.
        /// Calling base.Initialize will enumerate through any components and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            this.graphics.PreferredBackBufferWidth = Globals.WIDTH;
            this.graphics.PreferredBackBufferHeight = Globals.HEIGHT;
            this.graphics.IsFullScreen = false;
            this.graphics.ApplyChanges();

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            this.spriteBatch = new SpriteBatch(GraphicsDevice);
            LoadedContent.Load(this.Content);
            this.LoadLevels();
            this.LoadMenus();

            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world, checking for collisions,
        /// gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
            {
                GameRules.Pause();
            }

            switch (GameRules.State)
            {
                case GameState.Playing:
                    this.level.Update(gameTime);
                    break;

                case GameState.UpgradeMenu:
                    this.upgradeMenu.Update(gameTime);
                    break;

                case GameState.BossLevel:
                    this.bossLevel.Update(gameTime);
                    break;

                case GameState.Paused:
                    this.pauseMenu.Update(gameTime);
                    break;

                case GameState.MainMenu:
                    if (this.quitToMainMenuState != GameRules.QuitToMenuState)
                    {
                        this.LoadMainMenu();
                    }

                    this.mainMenu.Update(gameTime);
                    break;

                case GameState.GameComplete:
                    this.winMenu.Update(gameTime);
                    break;

                case GameState.GameOver:
                    this.loseMenu.Update(gameTime);
                    break;

                case GameState.Controls:
                    this.controlsMenu.Update(gameTime);
                    break;

                case GameState.BackStory:
                    this.backStoryMenu.Update(gameTime);
                    break;

                case GameState.Quit:
                    this.Exit();
                    break;
            }

            if (GameRules.NeedReloading())
            {
                LoadLevels();
                LoadMenus();
                GameRules.ReLoaded();
            }

            // TODO: Add your update logic here
            base.Update(gameTime);
        }

        /// <summary>
        /// Load the back story menu
        /// </summary>
        private void LoadBackStoryMenu()
        {
            this.backStoryMenu = new Menu();
            StaticSprite backStory = new StaticSprite(new Vector2(Globals.WIDTH / 2, Globals.HEIGHT / 2), MathHelper.ToRadians(0), LoadedContent.GetTexture("BackStory"));
            backStory.DisplayInfo.Color = Color.Green;
            this.backStoryMenu.AddStaticSprites(backStory);
            this.backStoryMenu.AddButton(new BasicMenuButton(new Vector2(Globals.WIDTH - 200, Globals.HEIGHT - 75), Color.Green, 0.7f, GameRules.MainMenu, LoadedContent.GetTexture("MainMenu")));
        }

        /// <summary>
        /// Load the controls menu
        /// </summary>
        private void LoadControlsMenu()
        {
            this.controlsMenu = new Menu();
            SpriteFont font = LoadedContent.GetFont("Title");
            this.controlsMenu.AddText(new MenuText("Controls", Color.Green, new Rectangle(400, 50, Globals.WIDTH - 800, 100), Alignment.Center, font));
            this.controlsMenu.AddStaticSprites(new StaticSprite(new Vector2(Globals.WIDTH / 2, Globals.HEIGHT / 2), MathHelper.ToRadians(0), LoadedContent.GetTexture("Instructions")));
            this.controlsMenu.AddButton(new BasicMenuButton(new Vector2(Globals.WIDTH / 2, Globals.HEIGHT - 75), Color.Green, 1, GameRules.MainMenu, LoadedContent.GetTexture("MainMenu")));
        }

        /// <summary>
        /// Load Levels
        /// </summary>
        private void LoadLevels()
        {
            this.level = new Level();
            this.bossLevel = new BossLevel(new Vector2(200, Globals.HEIGHT / 2));
        }

        /// <summary>
        /// Load the lose menu
        /// </summary>
        private void LoadLoseMenu()
        {
            this.loseMenu = new Menu();
            SpriteFont font = LoadedContent.GetFont("Title");
            this.loseMenu.AddText(new MenuText("Oh no! You died!", Color.Green, new Rectangle(400, 50, Globals.WIDTH - 800, 100), Alignment.Center, font));
            this.loseMenu.AddButton(new BasicMenuButton(new Vector2(Globals.WIDTH / 2, 300), Color.Green, 1, GameRules.NewGame, LoadedContent.GetTexture("NewGame")));
            this.loseMenu.AddButton(new BasicMenuButton(new Vector2(Globals.WIDTH / 2, 450), Color.Green, 1, GameRules.Quit, LoadedContent.GetTexture("Quit")));
        }

        /// <summary>
        /// Load the main menu
        /// </summary>
        private void LoadMainMenu()
        {
            // Button Positions
            Vector2 buttonPosition1of4 = new Vector2(Globals.WIDTH / 2, 200);
            Vector2 buttonPosition2of4 = new Vector2(Globals.WIDTH / 2, 350);
            Vector2 buttonPosition3of4 = new Vector2(Globals.WIDTH / 2, 500);
            Vector2 buttonPosition4of4 = new Vector2(Globals.WIDTH / 2, 650);
            Vector2 buttonPosition1of5 = new Vector2(Globals.WIDTH / 2, 200);
            Vector2 buttonPosition2of5 = new Vector2(Globals.WIDTH / 2, 310);
            Vector2 buttonPosition3of5 = new Vector2(Globals.WIDTH / 2, 420);
            Vector2 buttonPosition4of5 = new Vector2(Globals.WIDTH / 2, 530);
            Vector2 buttonPosition5of5 = new Vector2(Globals.WIDTH / 2, 640);

            this.mainMenu = new Menu();
            this.quitToMainMenuState = GameRules.QuitToMenuState;
            SpriteFont font = LoadedContent.GetFont("Title");
            this.mainMenu.AddText(new MenuText("Main Menu", Color.Green, new Rectangle(400, 50, Globals.WIDTH - 800, 100), Alignment.Center, font));
            if (!this.quitToMainMenuState.HasValue)
            {
                this.mainMenu.AddButton(new BasicMenuButton(buttonPosition1of4, Color.Green, 1, GameRules.NewGame, LoadedContent.GetTexture("NewGame")));
                this.mainMenu.AddButton(new BasicMenuButton(buttonPosition2of4, Color.Green, 1, GameRules.Controls, LoadedContent.GetTexture("ControlsButton")));
                this.mainMenu.AddButton(new BasicMenuButton(buttonPosition3of4, Color.Green, 1, GameRules.BackStory, LoadedContent.GetTexture("BackStoryButton")));
                this.mainMenu.AddButton(new BasicMenuButton(buttonPosition4of4, Color.Green, 1, GameRules.Quit, LoadedContent.GetTexture("Quit")));
            }
            else
            {
                Action resumeAction;
                switch (this.quitToMainMenuState)
                {
                    case GameState.Playing:
                        resumeAction = GameRules.Resume;
                        break;

                    case GameState.BossLevel:
                        resumeAction = GameRules.Resume;
                        break;

                    case GameState.UpgradeMenu:
                        resumeAction = GameRules.OpenUpgradeMenu;
                        break;

                    case GameState.Paused:
                        resumeAction = GameRules.Resume;
                        break;

                    default:
                        resumeAction = GameRules.Resume;
                        break;
                }

                this.mainMenu.AddButton(new BasicMenuButton(buttonPosition1of5, Color.Green, 1, resumeAction, LoadedContent.GetTexture("Resume")));
                this.mainMenu.AddButton(new BasicMenuButton(buttonPosition2of5, Color.Green, 1, GameRules.NewGame, LoadedContent.GetTexture("NewGame")));
                this.mainMenu.AddButton(new BasicMenuButton(buttonPosition3of5, Color.Green, 1, GameRules.Controls, LoadedContent.GetTexture("ControlsButton")));
                this.mainMenu.AddButton(new BasicMenuButton(buttonPosition4of5, Color.Green, 1, GameRules.BackStory, LoadedContent.GetTexture("BackStoryButton")));
                this.mainMenu.AddButton(new BasicMenuButton(buttonPosition5of5, Color.Green, 1, GameRules.Quit, LoadedContent.GetTexture("Quit")));
            }
        }

        /// <summary>
        /// Load Menus
        /// </summary>
        private void LoadMenus()
        {
            this.LoadMainMenu();
            this.LoadPauseMenu();
            this.LoadUpgradeMenu();
            this.LoadWinMenu();
            this.LoadLoseMenu();
            this.LoadControlsMenu();
            this.LoadBackStoryMenu();
        }

        /// <summary>
        /// Load the pause menu
        /// </summary>
        private void LoadPauseMenu()
        {
            this.pauseMenu = new Menu();
            SpriteFont font = LoadedContent.GetFont("Title");
            this.pauseMenu.AddText(new MenuText("Paused", Color.Green, new Rectangle(400, 50, Globals.WIDTH - 800, 100), Alignment.Center, font));
            this.pauseMenu.AddButton(new BasicMenuButton(new Vector2(Globals.WIDTH / 2, 300), Color.Green, 1, GameRules.Resume, LoadedContent.GetTexture("Resume")));
            this.pauseMenu.AddButton(new BasicMenuButton(new Vector2(Globals.WIDTH / 2, 450), Color.Green, 1, GameRules.MainMenu, LoadedContent.GetTexture("MainMenu")));
        }

        /// <summary>
        /// Load the upgrade menu
        /// </summary>
        private void LoadUpgradeMenu()
        {
            this.upgradeMenu = new Menu();
            float scale = 0.6f;
            List<Vector2> positions = new List<Vector2>();
            int columns = 5;
            int rows = 3;
            int leftOffset = 200;
            int rightOffset = 200;
            int bottomOffset = 200;
            int topOffset = 200;
            int width = Globals.WIDTH - leftOffset - rightOffset;
            int height = Globals.HEIGHT - bottomOffset - topOffset;
            int horizontalStep = width / (columns - 1);
            int verticalStep = height / (rows - 1);

            for (int column = 0; column < columns; column++)
            {
                for (int row = 0; row < rows; row++)
                {
                    positions.Add(new Vector2(leftOffset + (column * horizontalStep), topOffset + (row * verticalStep)));
                }
            }

            this.upgradeMenu.AddButton(new WeaponUpgradeMenuButton(positions[0], LoadedContent.GetTexture("BasicGun1"), scale, WeaponType.BasicGun, 0, 300));
            this.upgradeMenu.AddButton(new WeaponUpgradeMenuButton(positions[1], LoadedContent.GetTexture("BasicGun2"), scale, WeaponType.BasicGun, 1, 1000));
            this.upgradeMenu.AddButton(new WeaponUpgradeMenuButton(positions[2], LoadedContent.GetTexture("BasicGun3"), scale, WeaponType.BasicGun, 2, 5000));
            this.upgradeMenu.AddButton(new WeaponUpgradeMenuButton(positions[3], LoadedContent.GetTexture("Rockets1"), scale, WeaponType.RocketGun, 0, 1000));
            this.upgradeMenu.AddButton(new WeaponUpgradeMenuButton(positions[4], LoadedContent.GetTexture("Rockets2"), scale, WeaponType.RocketGun, 1, 3000));
            this.upgradeMenu.AddButton(new WeaponUpgradeMenuButton(positions[5], LoadedContent.GetTexture("Rockets3"), scale, WeaponType.RocketGun, 2, 6000));
            this.upgradeMenu.AddButton(new WeaponUpgradeMenuButton(positions[6], LoadedContent.GetTexture("DoubleGun1"), scale, WeaponType.DoubleGun, 0, 1500));
            this.upgradeMenu.AddButton(new WeaponUpgradeMenuButton(positions[7], LoadedContent.GetTexture("DoubleGun2"), scale, WeaponType.DoubleGun, 1, 3500));
            this.upgradeMenu.AddButton(new WeaponUpgradeMenuButton(positions[8], LoadedContent.GetTexture("DoubleGun3"), scale, WeaponType.DoubleGun, 2, 8000));
            this.upgradeMenu.AddButton(new PowerupUpgradeMenuButton(positions[9], LoadedContent.GetTexture("Health1"), scale, PowerupType.Health, 0, 1000));
            this.upgradeMenu.AddButton(new PowerupUpgradeMenuButton(positions[10], LoadedContent.GetTexture("Health2"), scale, PowerupType.Health, 1, 2000));
            this.upgradeMenu.AddButton(new PowerupUpgradeMenuButton(positions[11], LoadedContent.GetTexture("Health3"), scale, PowerupType.Health, 2, 3000));
            this.upgradeMenu.AddButton(new PowerupUpgradeMenuButton(positions[12], LoadedContent.GetTexture("Shield1"), scale, PowerupType.Shield, 0, 500));
            this.upgradeMenu.AddButton(new PowerupUpgradeMenuButton(positions[13], LoadedContent.GetTexture("Shield2"), scale, PowerupType.Shield, 1, 1250));
            this.upgradeMenu.AddButton(new PowerupUpgradeMenuButton(positions[14], LoadedContent.GetTexture("Shield3"), scale, PowerupType.Shield, 2, 2500));

            this.upgradeMenu.AddButton(new PlayBossMenuButton(new Vector2(Globals.WIDTH / 2, Globals.HEIGHT - 75), Color.Green, scale));
            this.upgradeMenu.AddButton(new BasicMenuButton(new Vector2(200, Globals.HEIGHT - 75), Color.Green, scale, GameRules.NextLevel, LoadedContent.GetTexture("FaceAnotherWave")));
            this.upgradeMenu.AddButton(new BasicMenuButton(new Vector2(Globals.WIDTH - 200, Globals.HEIGHT - 75), Color.Green, scale, GameRules.MainMenu, LoadedContent.GetTexture("MainMenu")));
        }

        /// <summary>
        /// Load the win menu
        /// </summary>
        private void LoadWinMenu()
        {
            this.winMenu = new Menu();
            SpriteFont font = LoadedContent.GetFont("Title");
            StaticSprite conclusion = new StaticSprite(new Vector2(Globals.WIDTH / 2, Globals.HEIGHT / 2), MathHelper.ToRadians(0), LoadedContent.GetTexture("Conclusion"));
            conclusion.DisplayInfo.Color = Color.Green;
            this.winMenu.AddStaticSprites(conclusion);
            this.winMenu.AddButton(new BasicMenuButton(new Vector2(Globals.WIDTH - 550, Globals.HEIGHT - 50), Color.Green, 0.6f, GameRules.NewGame, LoadedContent.GetTexture("NewGame")));
            this.winMenu.AddButton(new BasicMenuButton(new Vector2(Globals.WIDTH - 200, Globals.HEIGHT - 50), Color.Green, 0.6f, GameRules.Quit, LoadedContent.GetTexture("Quit")));
        }
    }
}