﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace GameDesign.Content
{
    /// <summary>
    /// All Content is Loaded into this class
    /// </summary>
    internal static class LoadedContent
    {
        /// <summary>
        /// The font to use if none can be found
        /// </summary>
        private static SpriteFont failureFont;

        /// <summary>
        /// The sounds to use if none can be found
        /// </summary>
        private static SoundEffect failureSound;

        /// <summary>
        /// The texture to use if none can be found
        /// </summary>
        private static Texture2D failureTexture;

        /// <summary>
        /// The Dictionary of fonts
        /// </summary>
        private static Dictionary<string, SpriteFont> fonts;

        /// <summary>
        /// The dictionary of sounds
        /// </summary>
        private static Dictionary<string, SoundEffect> sounds;

        /// <summary>
        /// The dictionary of textures
        /// </summary>
        private static Dictionary<string, Texture2D> textures;

        /// <summary>
        /// Get a specified font
        /// </summary>
        /// <param name="fontName">Name of the font</param>
        /// <returns>Requested font</returns>
        public static SpriteFont GetFont(string fontName)
        {
            SpriteFont toReturn;
            if (fonts.TryGetValue(fontName, out toReturn))
            {
                return toReturn;
            }
            else
            {
                return failureFont;
            }
        }

        /// <summary>
        /// Get a specified sound
        /// </summary>
        /// <param name="soundEffectName">Name of the sound</param>
        /// <returns>Requested sound</returns>
        public static SoundEffectInstance GetSoundEffect(string soundEffectName)
        {
            SoundEffect toReturn;
            if (sounds.TryGetValue(soundEffectName, out toReturn))
            {
                return toReturn.CreateInstance();
            }
            else
            {
                return failureSound.CreateInstance();
            }
        }

        /// <summary>
        /// Get a specified texture
        /// </summary>
        /// <param name="textureName">Name of the texture</param>
        /// <returns>Requested texture</returns>
        public static Texture2D GetTexture(string textureName)
        {
            Texture2D toReturn;
            if (textures.TryGetValue(textureName, out toReturn))
            {
                return toReturn;
            }
            else
            {
                return failureTexture;
            }
        }

        /// <summary>
        /// Load all relevant content
        /// </summary>
        /// <param name="content">The ContentManager object</param>
        public static void Load(ContentManager content)
        {
            // All Textures were drawn by myself
            textures = new Dictionary<string, Texture2D>();
            sounds = new Dictionary<string, SoundEffect>();
            fonts = new Dictionary<string, SpriteFont>();

            failureTexture = content.Load<Texture2D>("Images/Sprites/Player");
            failureSound = content.Load<SoundEffect>("Sounds/Thrusters");
            failureFont = content.Load<SpriteFont>("Fonts/Title");

            // Player Texture
            textures.Add("Player", content.Load<Texture2D>("Images/Sprites/PlayerShadow"));
            textures.Add("Enemy", content.Load<Texture2D>("Images/Sprites/Enemy"));

            // Particle Texture
            textures.Add("Particle", content.Load<Texture2D>("Images/Sprites/Pixel"));

            // Projectile Textures
            textures.Add("Bullet", content.Load<Texture2D>("Images/Sprites/Bullet"));
            textures.Add("Rocket", content.Load<Texture2D>("Images/Sprites/Rocket"));

            // GUI Textures
            textures.Add("HealthBar", content.Load<Texture2D>("Images/OSD/HealthBar"));
            textures.Add("RocketOSD", content.Load<Texture2D>("Images/OSD/RocketOSD"));
            textures.Add("BasicGunOSD", content.Load<Texture2D>("Images/OSD/BasicGunOSD"));
            textures.Add("DoubleGunOSD", content.Load<Texture2D>("Images/OSD/DoubleGunOSD"));
            textures.Add("AimArrow", content.Load<Texture2D>("Images/OSD/AimArrow"));

            // Full screen images
            textures.Add("Instructions", content.Load<Texture2D>("Images/Instructions"));
            textures.Add("BackStory", content.Load<Texture2D>("Images/BackStory"));
            textures.Add("Conclusion", content.Load<Texture2D>("Images/Conclusion"));

            // Powerup Textures
            textures.Add("HealthPack", content.Load<Texture2D>("Images/Sprites/HealthPack"));
            textures.Add("Shield", content.Load<Texture2D>("Images/Sprites/Shield"));

            // Overlay Textures
            textures.Add("ShieldOverlay", content.Load<Texture2D>("Images/Sprites/ShieldOverlay"));

            // Buttons
            textures.Add("ButtonOutline", content.Load<Texture2D>("Images/Buttons/ButtonOutline"));
            textures.Add("LongButtonOutline", content.Load<Texture2D>("Images/Buttons/LongButton"));
            textures.Add("ButtonOutlineCrossed", content.Load<Texture2D>("Images/Buttons/ButtonOutlineCrossed"));
            textures.Add("LongButtonOutlineCrossed", content.Load<Texture2D>("Images/Buttons/LongButtonCrossed"));
            textures.Add("Resume", content.Load<Texture2D>("Images/Buttons/Resume"));
            textures.Add("FaceAnotherWave", content.Load<Texture2D>("Images/Buttons/FaceAnotherWave"));
            textures.Add("NewGame", content.Load<Texture2D>("Images/Buttons/NewGame"));
            textures.Add("PlayBoss", content.Load<Texture2D>("Images/Buttons/PlayBoss"));
            textures.Add("Play", content.Load<Texture2D>("Images/Buttons/Play"));
            textures.Add("Quit", content.Load<Texture2D>("Images/Buttons/Quit"));
            textures.Add("ControlsButton", content.Load<Texture2D>("Images/Buttons/Controls"));
            textures.Add("BackStoryButton", content.Load<Texture2D>("Images/Buttons/WhyAmIHere"));
            textures.Add("MainMenu", content.Load<Texture2D>("Images/Buttons/MainMenu"));
            textures.Add("BasicGun1", content.Load<Texture2D>("Images/Buttons/UpgradeButtons/BasicGun1"));
            textures.Add("BasicGun2", content.Load<Texture2D>("Images/Buttons/UpgradeButtons/BasicGun2"));
            textures.Add("BasicGun3", content.Load<Texture2D>("Images/Buttons/UpgradeButtons/BasicGun3"));
            textures.Add("Rockets1", content.Load<Texture2D>("Images/Buttons/UpgradeButtons/Rockets1"));
            textures.Add("Rockets2", content.Load<Texture2D>("Images/Buttons/UpgradeButtons/Rockets2"));
            textures.Add("Rockets3", content.Load<Texture2D>("Images/Buttons/UpgradeButtons/Rockets3"));
            textures.Add("DoubleGun1", content.Load<Texture2D>("Images/Buttons/UpgradeButtons/DoubleGun1"));
            textures.Add("DoubleGun2", content.Load<Texture2D>("Images/Buttons/UpgradeButtons/DoubleGun2"));
            textures.Add("DoubleGun3", content.Load<Texture2D>("Images/Buttons/UpgradeButtons/DoubleGun3"));
            textures.Add("Shield1", content.Load<Texture2D>("Images/Buttons/UpgradeButtons/Shield1"));
            textures.Add("Shield2", content.Load<Texture2D>("Images/Buttons/UpgradeButtons/Shield2"));
            textures.Add("Shield3", content.Load<Texture2D>("Images/Buttons/UpgradeButtons/Shield3"));
            textures.Add("Health1", content.Load<Texture2D>("Images/Buttons/UpgradeButtons/Health1"));
            textures.Add("Health2", content.Load<Texture2D>("Images/Buttons/UpgradeButtons/Health2"));
            textures.Add("Health3", content.Load<Texture2D>("Images/Buttons/UpgradeButtons/Health3"));

            // Sounds Sourced from: www.freesoundeffects.com www.soundbible.com
            sounds.Add("Thrusters", content.Load<SoundEffect>("Sounds/Thrusters"));
            sounds.Add("GunShot", content.Load<SoundEffect>("Sounds/GunShot"));
            sounds.Add("Laser", content.Load<SoundEffect>("Sounds/Laser"));
            sounds.Add("Rocket", content.Load<SoundEffect>("Sounds/Rocket"));
            sounds.Add("Explosion1", content.Load<SoundEffect>("Sounds/Explosion1"));
            sounds.Add("Explosion2", content.Load<SoundEffect>("Sounds/Explosion2"));
            sounds.Add("Explosion3", content.Load<SoundEffect>("Sounds/Explosion3"));

            // Fonts
            fonts.Add("Title", content.Load<SpriteFont>("Fonts/Title"));
            fonts.Add("UpgradeCost", content.Load<SpriteFont>("Fonts/UpgradeCost"));
        }
    }
}